part of 'folders_bloc.dart';

@freezed
class FoldersState with _$FoldersState {
  factory FoldersState({
    @Default([]) List<FolderModel> folders,
    @Default([]) List<FeedModel> allFeeds,
    @Default({}) Map<int, List<FeedModel>> folderFeeds,
  }) = _FoldersState;
}
