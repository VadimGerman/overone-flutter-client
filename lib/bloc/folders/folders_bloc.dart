import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:overone/api/api_exception.dart';
import 'package:overone/api/folders_api.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/models/folder/folder_model.dart';

part 'folders_bloc.freezed.dart';

part 'folders_event.dart';

part 'folders_state.dart';

class FoldersBloc extends Bloc<FoldersEvent, FoldersState> {
  final AuthenticationModel authentication;
  final bool initLoadAllFeeds;
  final bool initLoadFolders;

  FoldersBloc({
    required this.authentication,
    this.initLoadAllFeeds = false,
    this.initLoadFolders = false,
  }) : super(FoldersState()) {
    on<FoldersGetAllFeedsEvent>(_getAllFeeds);
    on<FoldersGetFoldersEvent>(_getFolders);
    on<FoldersGetFolderFeedsEvent>(_getFolderFeeds);
    on<FoldersFeedUnsubscribeAllEvent>(_feedUnsubscribeAll);
    on<FoldersFeedUnsubscribeFolderEvent>(_feedUnsubscribeFolder);

    if (initLoadAllFeeds) add(FoldersEvent.getAllFeeds());
    if (initLoadFolders) add(FoldersEvent.getFolders());
  }

  void _getAllFeeds(FoldersGetAllFeedsEvent event, Emitter<FoldersState> emit) async {
    try {
      final feeds = await FoldersApi.getAllFolderFeeds(authentication: authentication);
      emit(state.copyWith(
        allFeeds: feeds,
      ));
    } on ApiException catch (e) {}
  }

  void _getFolders(FoldersGetFoldersEvent event, Emitter<FoldersState> emit) async {
    try {
      final folders = await FoldersApi.getFolders(authentication: authentication);
      folders.sort((a, b) => a.folderPosition.compareTo(b.folderPosition));

      emit(state.copyWith(
        folders: folders,
      ));
    } on ApiException catch (e) {}
  }

  void _getFolderFeeds(FoldersGetFolderFeedsEvent event, Emitter<FoldersState> emit) async {
    try {
      final folderFeeds = {...state.folderFeeds};
      folderFeeds[event.folderId] = await FoldersApi.getFolderFeeds(
        authentication: authentication,
        folderId: event.folderId,
      );
      emit(state.copyWith(
        folderFeeds: folderFeeds,
      ));
    } on ApiException catch (e) {}
  }

  void _feedUnsubscribeAll(FoldersFeedUnsubscribeAllEvent event, Emitter<FoldersState> emit) async {
    try {
      // Unsubscribe
      await FoldersApi.subscribeFeed(
        authentication: authentication,
        doSubscribe: false,
        feedId: event.feedId,
        foldersIds: [],
      );

      // Update allFeeds
      final allFeeds = state.allFeeds.where((feed) => feed.id != event.feedId).toList();

      // Update all folders
      add(FoldersEvent.getFolders());

      // Update folder feeds if contains unsubscribed feed
      final folderFeeds = {...state.folderFeeds};
      state.folderFeeds.forEach((folderId, feeds) {
        final filteredFeeds = feeds.where((feed) => feed.id == event.feedId).toList();
        if (filteredFeeds.length != feeds.length) {
          folderFeeds[folderId] = filteredFeeds;
        }
      });

      emit(state.copyWith(
        allFeeds: allFeeds,
        folderFeeds: folderFeeds,
      ));
    } on ApiException catch (e) {}
  }

  void _feedUnsubscribeFolder(FoldersFeedUnsubscribeFolderEvent event, Emitter<FoldersState> emit) async {
    try {
      // Unsubscribe
      await FoldersApi.subscribeFeed(
        authentication: authentication,
        doSubscribe: false,
        feedId: event.feedId,
        foldersIds: [event.folderId],
      );

      // Update folder feedsCount
      final folders = state.folders.map((folder) => folder.id == event.folderId ? folder.copyWith(feedsCount: folder.feedsCount - 1) : folder).toList();

      // Update folder feeds
      final folderFeeds = {...state.folderFeeds};
      if (state.folderFeeds[event.folderId] != null) {
        folderFeeds[event.folderId] = folderFeeds[event.folderId]!.where((feed) => feed.id != event.feedId).toList();
      }

      emit(state.copyWith(
        folderFeeds: folderFeeds,
        folders: folders,
      ));
    } on ApiException catch (e) {}
  }
}
