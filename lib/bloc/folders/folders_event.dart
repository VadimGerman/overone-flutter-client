part of 'folders_bloc.dart';

@freezed
class FoldersEvent with _$FoldersEvent {
  factory FoldersEvent.getAllFeeds() = FoldersGetAllFeedsEvent;

  factory FoldersEvent.getFolders() = FoldersGetFoldersEvent;

  factory FoldersEvent.getFolderFeeds(int folderId) = FoldersGetFolderFeedsEvent;

  factory FoldersEvent.feedUnsubscribeAll(int feedId) = FoldersFeedUnsubscribeAllEvent;

  factory FoldersEvent.feedUnsubscribeFolder(int feedId, int folderId) = FoldersFeedUnsubscribeFolderEvent;
}
