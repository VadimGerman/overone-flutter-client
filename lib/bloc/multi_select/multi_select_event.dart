part of 'multi_select_bloc.dart';

@freezed
class MultiSelectEvent<Item> with _$MultiSelectEvent<Item> {
  factory MultiSelectEvent.toggle(Item item) = MultiSelectToggleEvent;
}
