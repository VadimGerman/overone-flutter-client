import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

part 'multi_select_bloc.freezed.dart';

part 'multi_select_event.dart';

part 'multi_select_state.dart';


class MultiSelectBloc<Item> extends Bloc<MultiSelectEvent<Item>, MultiSelectState<Item>> {
  MultiSelectBloc({
    List<Item> items = const [],
  }) : super(MultiSelectState(items: items)) {
    on<MultiSelectToggleEvent<Item>>(_toggle);
  }

  Future<void> _toggle(MultiSelectToggleEvent<Item> event, Emitter<MultiSelectState> emit) async {
    if(state.items.contains(event.item)) {
      emit(
        state.copyWith(
          items: state.items.toList()..remove(event.item),
        ),
      );
    } else {
      emit(
        state.copyWith(
          items: state.items.toList()..add(event.item),
        ),
      );
    }
  }
}
