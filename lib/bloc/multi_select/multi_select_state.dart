part of 'multi_select_bloc.dart';

@freezed
class MultiSelectState<Item> with _$MultiSelectState<Item> {
  factory MultiSelectState({
    @Default([]) List<Item> items
  }) = _MultiSelectState;
}
