part of 'update_folder_form_bloc.dart';

@freezed
class UpdateFolderFormState with _$UpdateFolderFormState {
  factory UpdateFolderFormState({
    FolderModel? newFolder,
    @Default('') String title,
    String? titleError,
    @Default(true) bool isPublic,
    @Default('') String iconName,
    String? formError
  }) = _UpdateFolderFormState;
}
