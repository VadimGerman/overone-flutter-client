import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:overone/api/api_exception.dart';
import 'package:overone/api/folders_api.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/models/folder/folder_model.dart';

part 'update_folder_form_bloc.freezed.dart';

part 'update_folder_form_event.dart';

part 'update_folder_form_state.dart';

class UpdateFolderFormBloc extends Bloc<UpdateFolderFormEvent, UpdateFolderFormState> {
  final AuthenticationModel authentication;
  final AppLocalizations l10n;
  final FolderModel folder;

  UpdateFolderFormBloc({
    required this.authentication,
    required this.l10n,
    required this.folder,
  }) : super(UpdateFolderFormState(
    title: folder.title,
    iconName: folder.iconName,
    isPublic: folder.isPublic,
  )) {
    on<UpdateFolderFormChangeTitleEvent>(_changeTitle);
    on<UpdateFolderFormChangeIsPublicEvent>(_changeIsPublic);
    on<UpdateFolderFormChangeIconNameEvent>(_changeIconName);
    on<UpdateFolderFormResetEvent>(_reset);
    on<UpdateFolderFormSubmitEvent>(_submit);
  }

  String? _validateTitle(String value) {
    if (value.isEmpty) return l10n.emptyFieldError;
    return null;
  }

  void _changeTitle(UpdateFolderFormChangeTitleEvent event, Emitter<UpdateFolderFormState> emit) async {
    emit(state.copyWith(
      title: event.title,
      titleError: _validateTitle(event.title),
    ));
  }

  void _changeIsPublic(UpdateFolderFormChangeIsPublicEvent event, Emitter<UpdateFolderFormState> emit) async {
    emit(state.copyWith(
      isPublic: event.isPublic,
    ));
  }

  void _changeIconName(UpdateFolderFormChangeIconNameEvent event, Emitter<UpdateFolderFormState> emit) async {
    emit(state.copyWith(
      iconName: event.iconName,
    ));
  }

  void _reset(UpdateFolderFormResetEvent event, Emitter<UpdateFolderFormState> emit) async {
    emit(state.copyWith(
      title: folder.title,
      iconName: folder.iconName,
      isPublic: folder.isPublic,
    ));
  }

  void _submit(UpdateFolderFormSubmitEvent event, Emitter<UpdateFolderFormState> emit) async {
    final titleError = _validateTitle(state.title);

    final submitState = state.copyWith(
      titleError: titleError,
    );

    emit(submitState);

    if (titleError == null) {
      try {
        final newFolder = await FoldersApi.updateFolder(
          authentication: authentication,
          folderId: folder.id,
          title: state.title,
          iconName: state.iconName,
          isPublic: state.isPublic,
        );
        emit(submitState.copyWith(
          newFolder: newFolder,
        ));
      } on ApiException catch(e) {
        emit(submitState.copyWith(
          formError: e.message,
        ));
      }
    }
  }
}
