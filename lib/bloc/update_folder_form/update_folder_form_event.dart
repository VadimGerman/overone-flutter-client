part of 'update_folder_form_bloc.dart';

@freezed
class UpdateFolderFormEvent with _$UpdateFolderFormEvent {
  factory UpdateFolderFormEvent.changeTitle(String title) = UpdateFolderFormChangeTitleEvent;

  factory UpdateFolderFormEvent.changeIsPublic(bool isPublic) = UpdateFolderFormChangeIsPublicEvent;

  factory UpdateFolderFormEvent.changeIconName(String iconName) = UpdateFolderFormChangeIconNameEvent;

  factory UpdateFolderFormEvent.reset() = UpdateFolderFormResetEvent;

  factory UpdateFolderFormEvent.submit() = UpdateFolderFormSubmitEvent;
}
