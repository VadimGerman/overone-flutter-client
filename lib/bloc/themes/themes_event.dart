part of 'themes_bloc.dart';

@freezed
class ThemesEvent with _$ThemesEvent {
  factory ThemesEvent.setTheme({
    required Brightness brightness,
  }) = ThemesSetThemeEvent;
}
