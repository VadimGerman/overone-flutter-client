import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:overone/styles/app_theme.dart';
import 'package:overone/styles/themes/base_theme.dart';

part 'themes_bloc.freezed.dart';

part 'themes_event.dart';

part 'themes_state.dart';

class ThemesBloc extends HydratedBloc<ThemesEvent, ThemesState> {
  ThemesBloc({
    required Brightness brightness,
  }) : super(ThemesState(theme: AppTheme.build(brightness))) {
    on<ThemesSetThemeEvent>(_setTheme);
  }

  Future<void> _setTheme(ThemesSetThemeEvent event, Emitter<ThemesState> emit) async {
    emit(
      state.copyWith(
        theme: AppTheme.build(event.brightness),
      ),
    );
  }

  Brightness _brightnessFromJson(String json) {
    return json == 'light' ? Brightness.light : Brightness.dark;
  }

  @override
  ThemesState? fromJson(Map<String, dynamic> json) {
    final brightness = _brightnessFromJson(json['brightness']);
    final themeData = AppTheme.build(brightness);
    return ThemesState(theme: themeData);
  }

  @override
  Map<String, dynamic>? toJson(ThemesState state) {
    return {
      'brightness': state.theme.brightness == Brightness.light ? 'light' : 'dark',
    };
  }
}
