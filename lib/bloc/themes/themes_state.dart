part of 'themes_bloc.dart';

@freezed
class ThemesState with _$ThemesState {
  factory ThemesState({
    required ThemeData theme,
  }) = _ThemesState;
}
