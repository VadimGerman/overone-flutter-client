import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:overone/api/api_exception.dart';
import 'package:overone/api/actions_api.dart';
import 'package:overone/models/article_actions/article_actions_model.dart';
import 'package:overone/models/authentication/authentication_model.dart';

part 'article_actions_block.freezed.dart';

part 'article_actions_block.g.dart';

part 'article_actions_event.dart';

part 'article_actions_state.dart';

class ArticleActionsBloc extends Bloc<ArticleActionsEvent, ArticleActionsState> {
  final AuthenticationModel authentication;

  ArticleActionsBloc({
    required this.authentication,
  }) : super(ArticleActionsState()) {
    on<ArticleActionsAddToBookmarksEvent>(_addToBookmarks);
    on<ArticleActionsDeleteFromBookmarksEvent>(_deleteFromBookmarks);
    on<ArticleActionsAddToHistoryEvent>(_addToHistory);
    on<ArticleActionsDeleteFromHistoryEvent>(_deleteFromHistory);
  }

  Future<void> _addToBookmarks(ArticleActionsAddToBookmarksEvent event,
      Emitter<ArticleActionsState> emit) async {
    return _addToList(event, emit, "bookmarks_d3Sk9N");
  }

  Future<void> _deleteFromBookmarks(ArticleActionsDeleteFromBookmarksEvent event,
      Emitter<ArticleActionsState> emit) async {
    return _removeFromList(event, emit, "bookmarks_d3Sk9N");
  }

  Future<void> _addToHistory(ArticleActionsAddToHistoryEvent event,
      Emitter<ArticleActionsState> emit) async {
    return _addToList(event, emit, "history_d3Sk9N");
  }

  Future<void> _deleteFromHistory(ArticleActionsDeleteFromHistoryEvent event,
      Emitter<ArticleActionsState> emit) async {
    return _removeFromList(event, emit, "history_d3Sk9N");
  }

  Future<void> _addToList(ArticleActionsEvent event,
      Emitter<ArticleActionsState> emit, String listName) async {
    try {
      final actionsModel = await ActionsApi.addToList(
        authentication: authentication,
        listName: listName,
        articleId: event.articleId,
      );
      emit(
          state.copyWith(
            actionsModel: actionsModel
          )
      );
    } on ApiException catch (e) {
      emit(
        state.copyWith(
          error: e.message,
        ),
      );
    }
  }

  Future<void> _removeFromList(ArticleActionsEvent event,
      Emitter<ArticleActionsState> emit, String listName) async {
    try {
      final actionsModel = await ActionsApi.deleteFromList(
        authentication: authentication,
        listName: listName,
        articleId: event.articleId,
      );
      emit(
          state.copyWith(
              actionsModel: actionsModel
          )
      );
    } on ApiException catch (e) {
      emit(
        state.copyWith(
          error: e.message,
        ),
      );
    }
  }

  @override
  Map<String, dynamic>? toJson(ArticleActionsState state) {
    return state.toJson();
  }
}
