part of 'article_actions_block.dart';

@freezed
class ArticleActionsEvent with _$ArticleActionsEvent {
  factory ArticleActionsEvent({
    required int articleId,
  }) = _ArticleActionsEvent;


  factory ArticleActionsEvent.addToBookmarks({
    required int articleId,
  }) = ArticleActionsAddToBookmarksEvent;

  factory ArticleActionsEvent.deleteFromBookmarks({
    required int articleId,
  }) = ArticleActionsDeleteFromBookmarksEvent;

  factory ArticleActionsEvent.addToHistory({
    required int articleId,
  }) = ArticleActionsAddToHistoryEvent;

  factory ArticleActionsEvent.deleteFromHistory({
    required int articleId,
  }) = ArticleActionsDeleteFromHistoryEvent;
}
