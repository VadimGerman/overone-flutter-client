part of 'article_actions_block.dart';

@freezed
class ArticleActionsState with _$ArticleActionsState {
  factory ArticleActionsState({
    ArticleActionsModel? actionsModel,
    String? error,
  }) = _ArticleActionsState;

  factory ArticleActionsState.fromJson(Map<String, Object?> json) => _$ArticleActionsStateFromJson(json);
}
