import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:overone/api/api_exception.dart';
import 'package:overone/api/categories_api.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/models/category/category_model.dart';
import 'package:overone/models/feed/feed_model.dart';

part 'categories_bloc.freezed.dart';

part 'categories_bloc.g.dart';

part 'categories_event.dart';

part 'categories_state.dart';

class CategoriesBloc extends Bloc<CategoriesEvent, CategoriesState> {
  final int? parentId;
  final AuthenticationModel authentication;

  CategoriesBloc({
    required this.authentication,
    this.parentId,
    bool initLoad = true,
  }) : super(CategoriesState()) {
    on<CategoriesLoadEvent>(_load);
    on<CategoriesFetchSubCategoriesEvent>(_fetchSubCategories);
    on<CategoriesFetchFeedsEvent>(_fetchFeeds);

    if (initLoad) {
      add(CategoriesLoadEvent());
    }
  }

  Future<void> _load(CategoriesLoadEvent event, Emitter<CategoriesState> emit) async {
    try {
      final categories = parentId == null ? await CategoriesApi.getCategories() : await CategoriesApi.getSubCategories(parentId: parentId!);
      emit(
        state.copyWith(
          categories: categories,
        ),
      );
    } on ApiException catch (e) {
      emit(
        state.copyWith(
          error: e.message,
        ),
      );
    }
  }

  Future<void> _fetchSubCategories(CategoriesFetchSubCategoriesEvent event, Emitter<CategoriesState> emit) async {
    try {
      final Map<int, List<CategoryModel>> savedSubCategories = state.subCategories == null ? {} : {...state.subCategories!};
      final subCategories = await CategoriesApi.getSubCategories(parentId: event.parentId);
      savedSubCategories[event.parentId] = subCategories;
      emit(
        state.copyWith(
          subCategories: savedSubCategories,
        ),
      );
    } on ApiException catch (e) {
      emit(
        state.copyWith(
          error: e.message,
        ),
      );
    }
  }

  Future<void> _fetchFeeds(CategoriesFetchFeedsEvent event, Emitter<CategoriesState> emit) async {
    try {
        final feeds = await CategoriesApi.getCategoryFeeds(
          authentication: authentication,
          categoryId: event.parentId,
        );
        final Map<int, List<FeedModel>> savedFeeds = state.feeds == null ? {} : {...state.feeds!};
        savedFeeds[event.parentId] = feeds;
      emit(
        state.copyWith(
          feeds: savedFeeds,
        ),
      );
    } on ApiException catch (e) {
      emit(
        state.copyWith(
          error: e.message,
        ),
      );
    }
  }
}
