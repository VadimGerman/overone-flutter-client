part of 'categories_bloc.dart';

@freezed
class CategoriesState with _$CategoriesState {
  factory CategoriesState({
    List<CategoryModel>? categories,
    Map<int, List<CategoryModel>>? subCategories,
    Map<int, List<FeedModel>>? feeds,
    String? error
  }) = _CategoriesState;

  factory CategoriesState.fromJson(Map<String, Object?> json) => _$CategoriesStateFromJson(json);
}
