part of 'categories_bloc.dart';

@freezed
class CategoriesEvent with _$CategoriesEvent {
  factory CategoriesEvent.load() = CategoriesLoadEvent;
  factory CategoriesEvent.fetchSubCategories(int parentId) = CategoriesFetchSubCategoriesEvent;
  factory CategoriesEvent.fetchFeeds(int parentId) = CategoriesFetchFeedsEvent;
}
