import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:overone/api/api_exception.dart';
import 'package:overone/api/users_api.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/models/user_info/user_info_model.dart';

part 'authentication_bloc.freezed.dart';

part 'authentication_bloc.g.dart';

part 'authentication_event.dart';

part 'authentication_state.dart';

class AuthenticationBloc extends HydratedBloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc() : super(AuthenticationState()) {
    on<AuthenticationLoginEvent>(_login);
    on<AuthenticationLogoutEvent>(_logout);
  }

  Future<void> _login(AuthenticationLoginEvent event, Emitter<AuthenticationState> emit) async {
    try {
      final userInfo = await UsersApi.info(authentication: event.authentication);
      emit(
        state.copyWith(
          authentication: event.authentication,
          userInfo: userInfo,
        ),
      );
    } on ApiException catch (e) {
      emit(
        state.copyWith(
          error: e.message,
        ),
      );
    }
  }

  Future<void> _logout(AuthenticationLogoutEvent event, Emitter<AuthenticationState> emit) async {
    emit(
      AuthenticationState(),
    );
  }

  @override
  AuthenticationState? fromJson(Map<String, dynamic> json) {
    return AuthenticationState.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(AuthenticationState state) {
    return state.toJson();
  }
}
