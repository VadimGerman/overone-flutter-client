part of 'select_bloc.dart';

@freezed
class SelectEvent<Item> with _$SelectEvent<Item> {
  factory SelectEvent.set(Item? item) = SelectSetEvent;
}
