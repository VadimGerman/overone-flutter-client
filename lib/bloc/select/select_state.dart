part of 'select_bloc.dart';

@freezed
class SelectState<Item> with _$SelectState<Item> {
  factory SelectState({
    Item? item
  }) = _SelectState;
}
