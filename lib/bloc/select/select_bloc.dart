import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

part 'select_bloc.freezed.dart';

part 'select_event.dart';

part 'select_state.dart';


class SelectBloc<Item> extends Bloc<SelectEvent<Item>, SelectState<Item>> {
  SelectBloc({
    Item? item,
  }) : super(SelectState(item: item)) {
    on<SelectSetEvent<Item>>(_set);
  }

  Future<void> _set(SelectSetEvent event, Emitter<SelectState> emit) async {
    emit(
      state.copyWith(
        item: event.item,
      ),
    );
  }
}
