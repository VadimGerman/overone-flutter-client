import 'package:overone/api/articles_api.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/models/authentication/authentication_model.dart';

class ArticlesMyFeedBloc extends ArticlesBloc {
  final AuthenticationModel authentication;

  ArticlesMyFeedBloc({
    required this.authentication,
  }) : super(
          articlesOnPage: 10,
          loadCallback: (pagination) async {
            final news = await ArticlesApi.userNews(
              authentication: authentication,
              pagination: pagination,
            );
            return news.news;
          },
        );
}
