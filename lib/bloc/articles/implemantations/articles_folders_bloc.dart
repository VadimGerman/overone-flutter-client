import 'package:overone/api/articles_api.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/bloc/select/select_bloc.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/models/folder/folder_model.dart';

class ArticlesFoldersBloc extends ArticlesBloc {
  final AuthenticationModel authentication;
  final SelectBloc<int> selectBloc;

  ArticlesFoldersBloc({
    required this.authentication,
    required this.selectBloc,
  }) : super(
          hydrated: false,
          articlesOnPage: 10,
          loadCallback: (pagination) async {
            final selectedFolderId = selectBloc.state.item;
            if (selectedFolderId == null) {
              final news = await ArticlesApi.userNews(
                authentication: authentication,
                pagination: pagination,
              );
              return news.news;
            } else {
              final news = await ArticlesApi.folderArticles(
                folderId: selectedFolderId,
                authentication: authentication,
                pagination: pagination,
              );
              return news;
            }
          },
        );
}
