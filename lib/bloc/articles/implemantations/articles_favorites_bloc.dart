import 'package:overone/api/articles_api.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/models/authentication/authentication_model.dart';

class ArticlesHistoryBloc extends ArticlesBloc {
  final AuthenticationModel authentication;

  ArticlesHistoryBloc({
    required this.authentication,
  }) : super(
          articlesOnPage: 10,
          loadCallback: (pagination) async {
            final news = await ArticlesApi.listNews(
              authentication: authentication,
              listName: "history_d3Sk9N",
              pagination: pagination,
            );
            return news.news;
          },
        );
}
