import 'package:overone/api/articles_api.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/models/authentication/authentication_model.dart';

class ArticlesFavoritesBloc extends ArticlesBloc {
  final AuthenticationModel authentication;

  ArticlesFavoritesBloc({
    required this.authentication,
  }) : super(
          articlesOnPage: 10,
          loadCallback: (pagination) async {
            final news = await ArticlesApi.userRecomendations(
              authentication: authentication,
              pagination: pagination,
            );
            return news.news;
          },
        );
}
