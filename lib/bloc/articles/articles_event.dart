part of 'articles_bloc.dart';

@freezed
class ArticlesEvent with _$ArticlesEvent {
  factory ArticlesEvent.load() = ArticlesLoadEvent;
  factory ArticlesEvent.reset() = ArticlesResetEvent;

  factory ArticlesEvent.refresh() = ArticlesRefreshEvent;
}
