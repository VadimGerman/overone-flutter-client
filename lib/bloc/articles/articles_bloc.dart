import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:overone/api/api_exception.dart';
import 'package:overone/models/article/article_model.dart';
import 'package:overone/models/pagination/pagination_model.dart';

part 'articles_bloc.freezed.dart';

part 'articles_bloc.g.dart';

part 'articles_event.dart';

part 'articles_state.dart';


typedef ArticlesLoadCallback = Future<List<ArticleModel>> Function(PaginationModel pagination);

class ArticlesBloc extends HydratedBloc<ArticlesEvent, ArticlesState> {
  final int articlesOnPage;
  final ArticlesLoadCallback loadCallback;
  final bool initLoad;
  final bool hydrated;

  ArticlesBloc({
    required this.articlesOnPage,
    required this.loadCallback,
    this.initLoad = true,
    this.hydrated = true,
  }) : super(ArticlesState()) {
    on<ArticlesLoadEvent>(_load);
    on<ArticlesRefreshEvent>(_refresh);
    on<ArticlesResetEvent>(_reset);

    if(initLoad) {
      add(ArticlesLoadEvent());
    }
  }

  bool needLoadMore(int index) => index + 1 == _paginationOffset;

  int get _paginationOffset => state.currentPage * articlesOnPage;

  Future<void> _load(ArticlesLoadEvent event, Emitter<ArticlesState> emit) async {
    try {
      final isFirstLoad = state.articles == null || state.articles!.isEmpty;
      final firstId = isFirstLoad ? 0 : state.articles!.first.id;
      final lastId = isFirstLoad ? -1 : state.articles!.last.id;
      final articles = await loadCallback(
        PaginationModel(
          limit: articlesOnPage,
          offset: -1,
          firstId: firstId,
          lastId: lastId,
          sort: -1,
          order: -1,
        ),
      );
      emit(
        state.copyWith(
          articles: [
            ...?state.articles,
            ...articles,
          ],
          currentPage: state.currentPage + 1,
        ),
      );
    } on ApiException catch (e) {
      emit(
        state.copyWith(
          error: e.message,
        ),
      );
    }
  }

  Future<void> _refresh(ArticlesRefreshEvent event, Emitter<ArticlesState> emit) async {
    try {
      final articles = await loadCallback(
        PaginationModel(
          limit: articlesOnPage,
          offset: -1,
          firstId: 0,
          lastId: -1,
          sort: -1,
          order: -1,
        ),
      );

      emit(
        state.copyWith(
          articles: articles,
          currentPage: 1,
        ),
      );
    } on ApiException catch (e) {
      emit(
        state.copyWith(
          error: e.message,
        ),
      );
    }
  }
  Future<void> _reset(ArticlesResetEvent event, Emitter<ArticlesState> emit) async {
    emit(
      ArticlesState()
    );
  }

  @override
  ArticlesState? fromJson(Map<String, dynamic> json) {
    if(!kIsWeb && hydrated) {
      return ArticlesState.fromJson(json);
    }
  }

  @override
  Map<String, dynamic>? toJson(ArticlesState state) {
    if(!kIsWeb && hydrated)  {
      return state.toJson();
    }
  }
}
