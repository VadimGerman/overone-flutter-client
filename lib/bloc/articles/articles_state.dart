part of 'articles_bloc.dart';

@freezed
class ArticlesState with _$ArticlesState {
  factory ArticlesState({
    List<ArticleModel>? articles,
    @Default(0) int currentPage,
    String? error
  }) = _ArticlesState;

  factory ArticlesState.fromJson(Map<String, Object?> json) => _$ArticlesStateFromJson(json);
}
