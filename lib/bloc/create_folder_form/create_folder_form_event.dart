part of 'create_folder_form_bloc.dart';

@freezed
class CreateFolderFormEvent with _$CreateFolderFormEvent {
  factory CreateFolderFormEvent.changeTitle(String title) = CreateFolderFormChangeTitleEvent;

  factory CreateFolderFormEvent.changeIconName(String iconName) = CreateFolderFormChangeIconNameEvent;

  factory CreateFolderFormEvent.changeIsPublic(bool isPublic) = CreateFolderFormChangeIsPublicEvent;

  factory CreateFolderFormEvent.submit() = CreateFolderFormSubmitEvent;
}
