import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:overone/api/api_exception.dart';
import 'package:overone/api/folders_api.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/models/folder/folder_model.dart';

part 'create_folder_form_bloc.freezed.dart';

part 'create_folder_form_event.dart';

part 'create_folder_form_state.dart';

class CreateFolderFormBloc extends Bloc<CreateFolderFormEvent, CreateFolderFormState> {
  final AuthenticationModel authentication;
  final AppLocalizations l10n;

  CreateFolderFormBloc({
    required this.authentication,
    required this.l10n,
  }) : super(CreateFolderFormState()) {
    on<CreateFolderFormChangeTitleEvent>(_changeTitle);
    on<CreateFolderFormChangeIsPublicEvent>(_changeIsPublic);
    on<CreateFolderFormChangeIconNameEvent>(_changeIconName);
    on<CreateFolderFormSubmitEvent>(_submit);
  }

  String? _validateTitle(String value) {
    if (value.isEmpty) return l10n.emptyFieldError;
    return null;
  }

  void _changeTitle(CreateFolderFormChangeTitleEvent event, Emitter<CreateFolderFormState> emit) async {
    emit(state.copyWith(
      title: event.title,
      titleError: _validateTitle(event.title),
    ));
  }

  void _changeIsPublic(CreateFolderFormChangeIsPublicEvent event, Emitter<CreateFolderFormState> emit) async {
    emit(state.copyWith(
      isPublic: event.isPublic,
    ));
  }

  void _changeIconName(CreateFolderFormChangeIconNameEvent event, Emitter<CreateFolderFormState> emit) async {
    emit(state.copyWith(
      iconName: event.iconName,
    ));
  }

  void _submit(CreateFolderFormSubmitEvent event, Emitter<CreateFolderFormState> emit) async {
    final titleError = _validateTitle(state.title);

    final submitState = state.copyWith(
      titleError: titleError,
    );

    emit(submitState);

    if (titleError == null) {
      try {
        final newFolder = await FoldersApi.createFolder(
          authentication: authentication,
          title: state.title,
          iconName: state.iconName,
          isPublic: state.isPublic,
        );
        emit(submitState.copyWith(
          newFolder: newFolder,
        ));
      } on ApiException catch(e) {
        emit(submitState.copyWith(
          formError: e.message,
        ));
      }
    }
  }
}
