part of 'create_folder_form_bloc.dart';

@freezed
class CreateFolderFormState with _$CreateFolderFormState {
  factory CreateFolderFormState({
    FolderModel? newFolder,
    @Default('') String title,
    String? titleError,
    @Default(true) bool isPublic,
    @Default('') String iconName,
    String? formError
  }) = _CreateFolderFormState;
}
