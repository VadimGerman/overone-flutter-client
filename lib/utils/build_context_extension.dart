import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:overone/bloc/authentication/authentication_bloc.dart';
import 'package:overone/bloc/localization/localization_bloc.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/styles/app_sizes.dart';

extension BuildContextExtension on BuildContext {
  AppLocalizations get l10n {
    return AppLocalizations.of(this) ?? (throw Exception('AppLocalizations not found in current context'));
  }

  MediaQueryData get mediaQuery {
    return MediaQuery.of(this);
  }

  bool get isDesktop {
    return AppSizes.isDesktop(mediaQuery.size.width);
  }

  AuthenticationModel get authentication {
    return read<AuthenticationBloc>().state.authentication ?? (throw Exception('Not authenticated'));
  }

  Locale get locale {
    return Locale(read<LocalizationBloc>().state.languageCode);
  }


}
