import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/bloc/articles/implemantations/articles_favorites_bloc.dart';
import 'package:overone/screens/favorites/components/favorites_screen_sub_navigation_menu.dart';
import 'package:overone/screens/favorites/components/favorites_history_article_drop_menu.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/articles/article_preview/article_preview.dart';
import 'package:overone/widgets/articles/articles_grid.dart';
import 'package:overone/widgets/screen_wrapper/screen_wrapper.dart';


class FavoritesHistoryScreen extends StatelessWidget {
  const FavoritesHistoryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bloc(
      child: ScreenWrapper(
        headerChild: _HeaderChild(),
        child: _Body(),
      ),
    );
  }
}

class _Bloc extends StatelessWidget {
  final Widget child;

  const _Bloc({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ArticlesHistoryBloc(
        authentication: context.authentication,
      ),
      child: child,
    );
  }
}

class _HeaderChild extends StatelessWidget {
  const _HeaderChild({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FavoritesScreenSubNavigationMenu(
      selected: 1,
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        context.read<ArticlesHistoryBloc>().add(ArticlesEvent.refresh());
      },
      child: ArticlesGrid(
        articlesBloc: context.read<ArticlesHistoryBloc>(),
        articleBuilder: (context, article) {
          return ArticlePreview(
            article: article,
            actionButtons: [
              HistoryArticleDropMenu(article: article),
            ],
          );
        },
      ),
    );
  }
}
