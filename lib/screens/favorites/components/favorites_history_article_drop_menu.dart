import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:overone/bloc/article_actions/article_actions_block.dart';
import 'package:overone/models/article/article_model.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/articles/article_preview/components/article_preview_menu_button.dart';
import 'package:overone/widgets/buttons/drop_menu_button.dart';

class HistoryArticleDropMenu extends StatelessWidget {
  final ArticleModel article;

  const HistoryArticleDropMenu({
    Key? key,
    required this.article,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ArticlePreviewMenuButton(
      article: article,
      dropMenuButtonItems: [
        DropMenuButtonItem(
          child: Row(
            children: [
              Icon(Icons.share),
              const SizedBox(width: 20),
              Text(context.l10n.share),
            ],
          ),
          onPressed: () {
            FlutterShare.share(
              title: context.l10n.shareArticle,
              linkUrl: article.link,
            );
          },
        ),
        DropMenuButtonItem(
          child: Row(
            children: [
              Icon(
                Icons.source_outlined,
              ),
              const SizedBox(width: 20),
              Text(context.l10n.goToSource),
            ],
          ),
          onPressed: () {},
        ),
        DropMenuButtonItem(
          child: Row(
            children: [
              Icon(
                Icons.bookmark_outline_outlined,
                color: Colors.red,
              ),
              const SizedBox(width: 20),
              Text(context.l10n.addToBookmarks),
            ],
          ),
          onPressed: () {
            context.read<ArticleActionsBloc>().add(
                (ArticleActionsEvent.addToBookmarks(articleId: article.id))
            );
          },
        ),
        DropMenuButtonItem(
          child: Row(
            children: [
              Icon(
                Icons.delete_forever,
                color: Colors.red,
              ),
              const SizedBox(width: 20),
              Text(context.l10n.deleteFromHistory),
            ],
          ),
          onPressed: () {
            context.read<ArticleActionsBloc>().add(
                (ArticleActionsEvent.deleteFromHistory(articleId: article.id))
            );
          },
        ),
      ],
    );
  }
}
