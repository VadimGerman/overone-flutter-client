import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:flutter/material.dart';
import 'package:overone/router/app_router.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/sub_navigation_menu/components/sub_navigation_menu_item.dart';
import 'package:overone/widgets/sub_navigation_menu/sub_navigation_menu.dart';

class NewsScreenSubNavigationMenu extends StatelessWidget {
  final int selected;

  const NewsScreenSubNavigationMenu({
    Key? key,
    required this.selected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return SubNavigationMenu(
      items: [
        SubNavigationMenuItem(
          title: l10n.myNews.toUpperCase(),
          selected: selected == 0,
          onPressed: () {
            context.router.push(NewsMyFeedRoute());
          },
        ),
        SubNavigationMenuItem(
          title: l10n.recommendations.toUpperCase(),
          selected: selected == 1,
          onPressed: () {
            context.router.push(NewsFavoritesRoute());
          },
        ),
      ],
    );
  }
}
