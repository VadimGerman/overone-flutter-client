import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/bloc/articles/implemantations/articles_my_feed_bloc.dart';
import 'package:overone/screens/news/components/news_my_feed_article_drop_menu.dart';
import 'package:overone/screens/news/components/news_screen_sub_navigation_menu.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/articles/article_preview/article_preview.dart';
import 'package:overone/widgets/articles/articles_grid.dart';
import 'package:overone/widgets/screen_wrapper/screen_wrapper.dart';

class NewsMyFeedScreen extends StatelessWidget {
  const NewsMyFeedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bloc(
      child: ScreenWrapper(
        headerChild: _HeaderChild(),
        child: _Body(),
      ),
    );
  }
}

class _Bloc extends StatelessWidget {
  final Widget child;

  const _Bloc({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ArticlesMyFeedBloc(
        authentication: context.authentication,
      ),
      child: child,
    );
  }
}

class _HeaderChild extends StatelessWidget {
  const _HeaderChild({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NewsScreenSubNavigationMenu(
      selected: 0,
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        context.read<ArticlesMyFeedBloc>().add(ArticlesEvent.refresh());
      },
      child: ArticlesGrid(
        articlesBloc: context.read<ArticlesMyFeedBloc>(),
        articleBuilder: (context, article) {
          return ArticlePreview(
            article: article,
            actionButtons: [
              NewsMyFeedArticleDropMenu(article: article),
            ],
          );
        },
      ),
    );
  }
}
