import 'package:flutter/material.dart';
import 'package:overone/screens/categories/desktop/categories_screen_desktop.dart';
import 'package:overone/screens/categories/mobile/categories_screen_mobile.dart';
import 'package:overone/screens/categories/components/categories_screen_wrapper.dart';
import 'package:overone/utils/build_context_extension.dart';

class CategoriesScreen extends StatelessWidget {
  const CategoriesScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CategoriesScreenWrapper(
      child: context.isDesktop ? CategoriesScreenDesktop() : CategoriesScreenMobile(),
    );
  }
}
