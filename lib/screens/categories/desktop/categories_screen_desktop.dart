import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/api/articles_api.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/bloc/categories/categories_bloc.dart';
import 'package:overone/bloc/select/select_bloc.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/screens/categories/components/categories_screen_articles_list.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/categories/expanded_menu/categories_expanded_menu_desktop/categories_expanded_menu_desktop.dart';

class CategoriesScreenDesktop extends StatelessWidget {
  const CategoriesScreenDesktop({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bloc(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: 329,
            ),
            child: CategoriesExpandedMenuDesktop(
              feedSelected: (context, feed) {
                context.read<ArticlesBloc>().add(ArticlesEvent.reset());
                context.read<SelectBloc<FeedModel>>().add(SelectEvent.set(feed));
              },
            ),
          ),
          const SizedBox(width: 72),
          Expanded(
            child: BlocBuilder<SelectBloc<FeedModel>, SelectState>(
              builder: (context, state) {
                return state.item == null
                    ? Center(
                        child: Text(
                          context.l10n.feedNotSelected,
                        ),
                      )
                    : CategoriesScreenArticlesList();
              },
            ),
          ),
        ],
      ),
    );
  }
}


class _Bloc extends StatelessWidget {
  final Widget child;

  const _Bloc({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => CategoriesBloc(authentication: context.authentication),
        ),
        BlocProvider(
          create: (context) => SelectBloc<FeedModel>(),
        ),
        BlocProvider(
          create: (context) => ArticlesBloc(
            hydrated: false,
            articlesOnPage: 10,
            initLoad: false,
            loadCallback: (pagination) async {
              final r = await ArticlesApi.feedArticles(
                feedId: context.read<SelectBloc<FeedModel>>().state.item!.id,
                pagination: pagination,
              );
              return r.news;
            },
          ),
        ),
      ],
      child: BlocListener<SelectBloc<FeedModel>, SelectState>(
        listener: (context, state) {
          context.read<ArticlesBloc>().add(ArticlesEvent.refresh());
        },
        child: child,
      ),
    );
  }
}