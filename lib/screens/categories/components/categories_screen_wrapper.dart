import 'package:auto_route/auto_route.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/screen_wrapper/screen_wrapper.dart';

class CategoriesScreenWrapper extends StatelessWidget {
  final Widget child;

  const CategoriesScreenWrapper({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: ScreenWrapper(
        child: Padding(
          padding: EdgeInsets.only(
            bottom: context.isDesktop ? 71 : 0,
            left: 20,
            right: 20,
          ),
          child: Align(
            alignment: Alignment.topCenter,
            child: child,
          ),
        ),
      ),
    );
  }
}
