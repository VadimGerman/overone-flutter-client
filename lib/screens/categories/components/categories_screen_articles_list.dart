import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/widgets/articles/article_preview/article_preview.dart';
import 'package:overone/widgets/articles/article_preview/components/article_preview_favorite_button.dart';
import 'package:overone/widgets/articles/articles_list.dart';

class CategoriesScreenArticlesList extends StatelessWidget {
  const CategoriesScreenArticlesList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ArticlesList(
      articlesBloc: context.read<ArticlesBloc>(),
      articleBuilder: (context, article) {
        return ArticlePreview(
          article: article,
          actionButtons: [
            ArticlesPreviewFavoriteButton(
              article: article,
            ),
          ],
        );
      },
    );
  }
}
