import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/categories/categories_bloc.dart';
import 'package:overone/router/app_router.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/categories/categories_list/categories_list.dart';

class CategoriesScreenMobile extends StatelessWidget {

  const CategoriesScreenMobile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bloc(
      child: Center(
        child: BlocBuilder<CategoriesBloc, CategoriesState>(
          builder: (context, state) {
            return CategoriesList(
              categories: state.categories,
              onPressed: (category) {
                context.router.push(SubCategoriesMobileRoute(categoryId: '${category.id}'));
              },
            );
          },
        ),
      ),
    );
  }
}


class _Bloc extends StatelessWidget {
  final Widget child;

  const _Bloc({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => CategoriesBloc(authentication: context.authentication),
        ),
      ],
      child: child,
    );
  }
}