import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/api/articles_api.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/screens/categories/components/categories_screen_articles_list.dart';
import 'package:overone/screens/categories/components/categories_screen_wrapper.dart';

class CategoryFeedScreen extends StatelessWidget {
  final String categoryId;
  final String subCategoryId;
  final String feedId;

  const CategoryFeedScreen({
    Key? key,
    @PathParam('categoryId') required this.categoryId,
    @PathParam('subCategoryId') required this.subCategoryId,
    @PathParam('feedId') required this.feedId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: this,
      child: _Bloc(
        child: CategoriesScreenWrapper(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: 420,
            ),
            child: CategoriesScreenArticlesList(),
          ),
        ),
      ),
    );
  }
}

class _Bloc extends StatelessWidget {
  final Widget child;

  const _Bloc({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final props = context.read<CategoryFeedScreen>();
    final feedId = int.parse(props.feedId);
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ArticlesBloc(
            articlesOnPage: 10,
            loadCallback: (pagination) async {
              final r = await ArticlesApi.feedArticles(
                feedId: feedId,
                pagination: pagination,
              );
              return r.news;
            },
          ),
        ),
      ],
      child: child,
    );
  }
}