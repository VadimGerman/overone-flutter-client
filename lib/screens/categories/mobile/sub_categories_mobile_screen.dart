import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/categories/categories_bloc.dart';
import 'package:overone/screens/categories/components/categories_screen_wrapper.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/categories/expanded_menu/sub_categories_expanded_menu_mobile/sub_categories_expanded_menu_mobile.dart';

class SubCategoriesMobileScreen extends StatelessWidget {
  final String categoryId;

  const SubCategoriesMobileScreen({
    Key? key,
    @PathParam('categoryId') required this.categoryId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: this,
      child: _Bloc(
        child: CategoriesScreenWrapper(
          child: SubCategoriesExpandedMenuMobile(
            categoryId: int.parse(categoryId),
            feedSelected: (context, subCategory, feed) {
              context.router.pushNamed('/category/${categoryId}/subcategory/${subCategory.id}/feeds/${feed.id}');
            },
          ),
        ),
      ),
    );
  }
}

class _Bloc extends StatelessWidget {
  final Widget child;

  const _Bloc({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final props = context.read<SubCategoriesMobileScreen>();
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              CategoriesBloc(
                authentication: context.authentication,
                parentId: int.parse(props.categoryId),
              ),
        ),
      ],
      child: child,
    );
  }
}
