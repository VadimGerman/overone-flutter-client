import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/authentication/authentication_bloc.dart';
import 'package:overone/widgets/screen_wrapper/screen_wrapper.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenWrapper(
      child: Center(
        child: ElevatedButton(
          child: Text('Logout'),
          onPressed: () {
            context.read<AuthenticationBloc>().add(AuthenticationEvent.logout());
          },
        ),
      ),
    );
  }
}