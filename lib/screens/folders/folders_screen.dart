import 'package:flutter/material.dart';
import 'package:overone/screens/folders/desktop/desktop_screen.dart';
import 'package:overone/screens/folders/mobile/folders_list_mobile_screen.dart';
import 'package:overone/utils/build_context_extension.dart';

class FoldersScreen extends StatelessWidget {
  const FoldersScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return context.isDesktop ? FoldersDesktopScreen() : FoldersListMobileScreen();
  }
}
