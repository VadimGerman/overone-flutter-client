import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/folders/folders_bloc.dart';
import 'package:overone/router/app_router.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/folders/dialogs/create_folder_dialog.dart';
import 'package:overone/widgets/folders/menu_items/add_folder_item.dart';
import 'package:overone/widgets/folders/menu_items/all_feeds_folder_item.dart';
import 'package:overone/widgets/folders/menu_items/folder_item.dart';
import 'package:overone/widgets/screen_wrapper/screen_wrapper.dart';

class FoldersListMobileScreen extends StatelessWidget {
  const FoldersListMobileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bloc(
      child: ScreenWrapper(
        child: Builder(builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      BlocBuilder<FoldersBloc, FoldersState>(
                        buildWhen: (p, n) => p.allFeeds.length != n.allFeeds.length,
                        builder: (context, state) {
                          return MouseRegion(
                            cursor: SystemMouseCursors.click,
                            child: GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {
                                context.router.push(FolderMobileRoute());
                              },
                              child: AllFeedsFolderMenuItem(
                                feedsCount: state.allFeeds.length,
                              ),
                            ),
                          );
                        },
                      ),
                      MouseRegion(
                        cursor: SystemMouseCursors.click,
                        child: GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            final providersContext = context;
                            final rootContext = context.read<BuildContext>();
                            showDialog(
                              context: rootContext,
                              builder: (context) {
                                return CreateFolderDialog(
                                  rootContext: rootContext,
                                  onSuccess: (folder) {
                                    providersContext.read<FoldersBloc>().add(FoldersEvent.getFolders());
                                  },
                                );
                              },
                            );
                          },
                          child: AddFolderMenuItem(),
                        ),
                      ),
                    ],
                  ),
                  BlocBuilder<FoldersBloc, FoldersState>(
                    buildWhen: (p, n) => p.folders != n.folders,
                    builder: (context, state) {
                      return Column(
                        children: [
                          for (final folder in state.folders) ...[
                            SizedBox(
                              height: 18,
                            ),
                            MouseRegion(
                              cursor: SystemMouseCursors.click,
                              child: GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: () {
                                  context.router.replace(FolderMobileRoute(folderId: folder.id.toString()));
                                },
                                child: FolderMenuItem(
                                  folder: folder,
                                ),
                              ),
                            ),
                          ],
                        ],
                      );
                    },
                  )
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}

class _Bloc extends StatelessWidget {
  final Widget child;

  const _Bloc({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => FoldersBloc(
            authentication: context.authentication,
            initLoadAllFeeds: true,
            initLoadFolders: true,
          ),
        ),
      ],
      child: child,
    );
  }
}
