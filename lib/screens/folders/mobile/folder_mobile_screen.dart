import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/bloc/articles/implemantations/articles_folders_bloc.dart';
import 'package:overone/bloc/folders/folders_bloc.dart';
import 'package:overone/bloc/select/select_bloc.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/router/app_router.dart';
import 'package:overone/screens/news/components/news_my_feed_article_drop_menu.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/articles/article_preview/article_preview.dart';
import 'package:overone/widgets/articles/articles_list.dart';
import 'package:overone/widgets/folders/menu_items/feed_item.dart';
import 'package:overone/widgets/screen_wrapper/screen_wrapper.dart';

class FolderMobileScreen extends StatefulWidget {
  final String? folderId;

  const FolderMobileScreen({
    Key? key,
    @PathParam('folderId') this.folderId,
  }) : super(key: key);

  @override
  State<FolderMobileScreen> createState() => _FolderMobileScreenState();
}

class _FolderMobileScreenState extends State<FolderMobileScreen> with SingleTickerProviderStateMixin {
  late final TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final folderId = widget.folderId == null ? null : int.parse(widget.folderId!);
    return WillPopScope(
      onWillPop: () async {
        context.router.replace(FoldersRoute());
        return false;
      },
      child: _Bloc(
        folderId: folderId,
        child: ScreenWrapper(
          child: Builder(builder: (context) {
            return Column(
              children: [
                TabBar(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _tabController,
                  tabs: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          context.l10n.feeds.toUpperCase(),
                          style: TextStyle(
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          context.l10n.news.toUpperCase(),
                          style: TextStyle(
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: TabBarView(
                      physics: NeverScrollableScrollPhysics(),
                      controller: _tabController,
                      children: [
                        BlocBuilder<FoldersBloc, FoldersState>(
                          builder: (context, state) {
                            List<FeedModel> feeds = [];
                            if (widget.folderId == null) {
                              context.read<FoldersBloc>().add(FoldersEvent.getAllFeeds());
                              feeds = state.allFeeds;
                            } else {
                              final folderId = int.parse(widget.folderId!);
                              context.read<FoldersBloc>().add(FoldersEvent.getFolderFeeds(folderId));
                              feeds = state.folderFeeds[folderId] ?? [];
                            }
                            return Column(
                              children: [
                                for (final feed in feeds) ...[
                                  FeedMenuItem(
                                    feed: feed,
                                    folderId: folderId,
                                    onUnsubscribe: () {
                                      context.read<ArticlesFoldersBloc>().add(ArticlesEvent.refresh());
                                    },
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ],
                            );
                          },
                        ),
                        ArticlesList(
                          articlesBloc: context.read<ArticlesFoldersBloc>(),
                          articleBuilder: (context, article) {
                            return ArticlePreview(
                              article: article,
                              actionButtons: [
                                NewsMyFeedArticleDropMenu(article: article),
                              ],
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            );
          }),
        ),
      ),
    );
  }
}

class _Bloc extends StatelessWidget {
  final Widget child;
  final int? folderId;

  const _Bloc({
    Key? key,
    required this.child,
    required this.folderId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => FoldersBloc(
            authentication: context.authentication,
          ),
        ),
        BlocProvider(
          create: (context) => SelectBloc<int>(
            item: folderId,
          ),
        ),
        BlocProvider(
          create: (context) => ArticlesFoldersBloc(
            authentication: context.authentication,
            selectBloc: context.read<SelectBloc<int>>(),
          ),
        ),
      ],
      child: child,
    );
  }
}
