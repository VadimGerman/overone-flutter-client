import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/bloc/articles/implemantations/articles_folders_bloc.dart';
import 'package:overone/bloc/folders/folders_bloc.dart';
import 'package:overone/bloc/select/select_bloc.dart';
import 'package:overone/screens/news/components/news_my_feed_article_drop_menu.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/articles/article_preview/article_preview.dart';
import 'package:overone/widgets/articles/articles_grid.dart';
import 'package:overone/widgets/folders/folders_expanded_menu/folders_expanded_menu_desktop.dart';
import 'package:overone/widgets/screen_wrapper/screen_wrapper.dart';

class FoldersDesktopScreen extends StatelessWidget {
  const FoldersDesktopScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bloc(
      child: Builder(builder: (context) {
        return ScreenWrapper(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 338,
                child: FoldersExpandedMenuDesktop(
                  allFeedSelected: (context) {
                    context.read<SelectBloc<int>>().add(SelectEvent.set(null));
                  },
                  folderSelected: (context, folder) {
                    context.read<SelectBloc<int>>().add(SelectEvent.set(folder.id));
                  },
                ),
              ),
              BlocBuilder<SelectBloc<int>, SelectState>(
                builder: (context, state) {
                  return Expanded(
                    child: RefreshIndicator(
                      onRefresh: () async {
                        context.read<ArticlesBloc>().add(ArticlesEvent.refresh());
                      },
                      child: ArticlesGrid(
                        articlesBloc: context.read<ArticlesFoldersBloc>(),
                        articleBuilder: (context, article) {
                          return ArticlePreview(
                            article: article,
                            actionButtons: [
                              NewsMyFeedArticleDropMenu(article: article),
                            ],
                          );
                        },
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        );
      }),
    );
  }
}

class _Bloc extends StatelessWidget {
  final Widget child;

  const _Bloc({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => FoldersBloc(
            authentication: context.authentication,
            initLoadAllFeeds: true,
            initLoadFolders: true,
          ),
        ),
        BlocProvider(
          create: (context) => SelectBloc<int>(),
        ),
        BlocProvider(
          create: (context) => ArticlesFoldersBloc(
            authentication: context.authentication,
            selectBloc: context.read<SelectBloc<int>>(),
          ),
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<SelectBloc<int>, SelectState>(
            listenWhen: (p, n) => p.item != n.item,
            listener: (context, state) {
              context.read<ArticlesFoldersBloc>().add(ArticlesEvent.refresh());
            },
          )
        ],
        child: child,
      ),
    );
  }
}
