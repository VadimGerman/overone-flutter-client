import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:overone/api/api_config.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/models/category/category_model.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/models/pagination/pagination_model.dart';

class CategoriesApi {
  static Future<List<CategoryModel>> getCategories() async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'categories/',
      ),
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return [
        for (final json in data['categories']) CategoryModel.fromJson(json),
      ];
    } else {
      throw Exception(data["message"] ?? 'Get categories error ${response.statusCode}');
    }
  }

  static Future<List<CategoryModel>> getSubCategories({
    required int parentId,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'categories/$parentId',
      ),
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return [
        for (final json in data['categories']) CategoryModel.fromJson(json),
      ];
    } else {
      throw Exception(data["message"] ?? 'Get subcategories error ${response.statusCode}');
    }
  }

  static Future<List<FeedModel>> getCategoryFeeds({
    required AuthenticationModel authentication,
    required int categoryId,
    PaginationModel? pagination,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'categories/$categoryId/feeds',
        queryParameters: {
          ...?(pagination?.toJsonWithStringTypes()),
          'userid': '${authentication.userId}',
        },
      ),
      headers: {
        'authorization': authentication.token,
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return [
        for (final json in data['feeds']) FeedModel.fromJson(json),
      ];
    } else {
      throw Exception(data["message"] ?? 'Get subcategory feeds error ${response.statusCode}');
    }
  }
}
