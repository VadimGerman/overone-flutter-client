import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:overone/api/api_config.dart';
import 'package:overone/api/api_exception.dart';
import 'package:overone/models/article/article_model.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/models/feed_news/feed_news_model.dart';
import 'package:overone/models/news/news_model.dart';
import 'package:overone/models/pagination/pagination_model.dart';

class ArticlesApi {
  static Future<NewsModel> userNews({
    required AuthenticationModel authentication,
    PaginationModel? pagination,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/news',
        queryParameters: {
          ...?pagination?.toJsonWithStringTypes(),
        },
      ),
      headers: {
        'authorization': authentication.token,
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return NewsModel.fromJson(data);
    } else {
      throw ApiException(message: data["message"] ?? 'Get user news error ${response.statusCode}');
    }
  }

  static listNews({
    required AuthenticationModel authentication,
    required String listName,
    PaginationModel? pagination,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/lists/$listName',
        queryParameters: {
          ...?pagination?.toJsonWithStringTypes(),
        },
      ),
      headers: {
        'authorization': authentication.token,
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return NewsModel.fromJson(data);
    } else {
      throw ApiException(message: data["message"] ?? 'Get user listNews error ${response.statusCode}');
    }
  }

  static userRecomendations({
    required AuthenticationModel authentication,
    PaginationModel? pagination,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/recomendations/',
        queryParameters: {
          ...?pagination?.toJsonWithStringTypes(),
        },
      ),
      headers: {
        'authorization': authentication.token,
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return NewsModel.fromJson(data);
    } else {
      throw ApiException(message: data["message"] ?? 'Get user news error ${response.statusCode}');
    }
  }

  static Future<FeedNewsModel> feedArticles({
    required int feedId,
    PaginationModel? pagination,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'feeds/$feedId/articles',
        queryParameters: {
          ...?pagination?.toJsonWithStringTypes(),
        },
      ),
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return FeedNewsModel.fromJson(data);
    } else {
      throw Exception(data["message"] ?? 'Get feed articles error ${response.statusCode}');
    }
  }

  static Future<List<ArticleModel>> folderArticles({
    required AuthenticationModel authentication,
    required int folderId,
    PaginationModel? pagination,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/folders/$folderId/news',
        queryParameters: {
          ...?pagination?.toJsonWithStringTypes(),
        },
      ),
      headers: {
        'authorization': authentication.token,
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return [
        for(final json in data['news'])
          ArticleModel.fromJson(json),
      ];
    } else {
      throw Exception(data["message"] ?? 'folderArticles error ${response.statusCode}');
    }
  }
}