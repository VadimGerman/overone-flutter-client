import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:overone/api/api_config.dart';
import 'package:overone/models/article_actions/article_actions_model.dart';
import 'package:overone/models/authentication/authentication_model.dart';

class ActionsApi {
  static addToList({
    required AuthenticationModel authentication,
    required int articleId,
    required String listName,
  }) async {
    return _articleAction(
      authentication: authentication,
      type: "AddToList",
      articleId: articleId,
      listName: listName,
    );
  }

  static deleteFromList({
    required AuthenticationModel authentication,
    required int articleId,
    required String listName,
  }) async {
    return _articleAction(
      authentication: authentication,
      type: "DeleteFromList",
      articleId: articleId,
      listName: listName,
    );
  }

  static _articleAction({
    required AuthenticationModel authentication,
    required int articleId,
    required String type,
    required String listName,
  }) async {
    final response = await http.post(
      ApiConfig.uri.replace(
        path: 'articles/$articleId/',
      ),
      headers: {
        'authorization': authentication.token,
      },
      body: {
        'json': jsonEncode(
          {
            'userId': '${authentication.userId}',
            'type': type,
            'listName': listName,
          },
        ),
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return ArticleActionsModel(success: data["success"] == "true");
    } else {
      throw Exception(data["message"] ?? 'Get feed articles error ${response.statusCode}');
    }
  }
}
