import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:overone/api/api_config.dart';
import 'package:crypto/crypto.dart';
import 'package:overone/api/api_exception.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/models/user_info/user_info_model.dart';

class UsersApi {
  static Future<AuthenticationModel> login({
    required String login,
    required String password,
  }) async {
    final response = await http.post(
      ApiConfig.uri.replace(
        path: 'users/login',
      ),
      body: {
        'json': jsonEncode(
          {
            'login': login,
            'passwordHash': md5.convert(utf8.encode(password)).toString(),
          },
        ),
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return AuthenticationModel.fromJson(data);
    } else {
      throw ApiException(message: data["message"] ?? 'Login error ${response.statusCode}');
    }
  }

  static Future<AuthenticationModel> signUp({
    required String login,
    required String password,
  }) async {
    final response = await http.post(
      ApiConfig.uri.replace(
        path: 'users',
      ),
      body: {
        'json': jsonEncode(
          {
            'login': login,
            'passwordHash': md5.convert(utf8.encode(password)).toString(),
            'avatar': '',
          },
        ),
      },
    );

    final data = jsonDecode(response.body);
    if (data['success']) {
      return AuthenticationModel.fromJson(data);
    } else {
      throw ApiException(message: data["message"] ?? 'Sign up error ${response.statusCode}');
    }
  }

  static Future<UserInfoModel> info({
    required AuthenticationModel authentication,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(path: 'users/${authentication.userId}'),
      headers: {
        'authorization': authentication.token,
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return UserInfoModel.fromJson(data['userInfo']);
    } else {
      throw ApiException(message: data["message"] ?? 'Get user info error ${response.statusCode}');
    }
  }
}
