import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:overone/api/api_config.dart';
import 'package:overone/api/api_exception.dart';
import 'package:overone/models/authentication/authentication_model.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/models/folder/folder_model.dart';

class FoldersApi {
  static Future<List<FolderModel>> getFolders({
    required AuthenticationModel authentication,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/folders',
      ),
      headers: {
        'authorization': authentication.token,
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return [for (final json in data['folders']) FolderModel.fromJson(json)];
    } else {
      throw ApiException(message: data["message"] ?? 'getFolders error ${response.statusCode}');
    }
  }

  static Future<FolderModel> createFolder({
    required AuthenticationModel authentication,
    required String title,
    required String iconName,
    required bool isPublic,
  }) async {
    final response = await http.post(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/folders',
      ),
      headers: {
        'authorization': authentication.token,
      },
      body: {
        'json': jsonEncode(
          {
            'title': title,
            'iconName': iconName,
            'isPublic': '$isPublic',
          },
        )
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return FolderModel.fromJson(data['folder']);
    } else {
      throw ApiException(message: data["message"] ?? 'createFolder error ${response.statusCode}');
    }
  }

  static Future<FolderModel> updateFolder({
    required AuthenticationModel authentication,
    required int folderId,
    String? title,
    String? iconName,
    bool? isPublic,
  }) async {
    final response = await http.put(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/folders',
      ),
      headers: {
        'authorization': authentication.token,
      },
      body: {
        'json': jsonEncode({
          'id': '$folderId',
          if (title != null) 'title': title,
          if (iconName != null) 'iconName': iconName,
          if (isPublic != null) 'isPublic': '$isPublic',
        })
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return FolderModel.fromJson(data['folder']);
    } else {
      throw ApiException(message: data["message"] ?? 'updateFolder error ${response.statusCode}');
    }
  }

  static Future<void> deleteFolder({
    required AuthenticationModel authentication,
    required int folderId,
  }) async {
    final response = await http.delete(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/folders/$folderId/',
      ),
      headers: {
        'authorization': authentication.token,
      },
    );
  }

  static Future<void> subscribeFeed({
    required AuthenticationModel authentication,
    required bool doSubscribe,
    required int feedId,
    required List<int> foldersIds,
  }) async {
    final response = await http.post(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/folderfeeds/',
      ),
      headers: {
        'authorization': authentication.token,
      },
      body: {
        'json': jsonEncode({
          'doSubscribe': '$doSubscribe',
          'feedId': '$feedId',
          'folders': foldersIds.map((folderId) => ({'id': '$folderId'})).toList(),
        })
      },
    );
  }

  static Future<List<FeedModel>> getFolderFeeds({
    required AuthenticationModel authentication,
    required int folderId,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/folders/$folderId/feeds',
      ),
      headers: {
        'authorization': authentication.token,
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return [
        for (final json in data['feeds']) FeedModel.fromJson(json),
      ];
    } else {
      throw ApiException(message: data["message"] ?? 'getFolderFeeds error ${response.statusCode}');
    }
  }

  static Future<List<FeedModel>> getAllFolderFeeds({
    required AuthenticationModel authentication,
  }) async {
    final response = await http.get(
      ApiConfig.uri.replace(
        path: 'users/${authentication.userId}/folders/feeds',
      ),
      headers: {
        'authorization': authentication.token,
      },
    );

    final data = jsonDecode(response.body);
    if (response.statusCode < 400) {
      return [
        for (final json in data['feeds']) FeedModel.fromJson(json),
      ];
    } else {
      throw ApiException(message: data["message"] ?? 'getAllFolderFeeds error ${response.statusCode}');
    }
  }
}
