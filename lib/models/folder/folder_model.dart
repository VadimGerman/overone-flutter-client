import 'package:freezed_annotation/freezed_annotation.dart';

part 'folder_model.freezed.dart';

part 'folder_model.g.dart';

@freezed
class FolderModel with _$FolderModel {
  factory FolderModel({
    required int id,
    required int feedsCount,
    required int folderPosition,
    required String iconName,
    required String title,
    required bool isPublic,
  }) = _FolderModel;

  factory FolderModel.fromJson(Map<String, Object?> json) => _$FolderModelFromJson(json);
}
