import 'package:freezed_annotation/freezed_annotation.dart';

part 'article_model.freezed.dart';

part 'article_model.g.dart';

@freezed
class ArticleModel with _$ArticleModel {
  factory ArticleModel({
    required String category,
    required String creator,
    required String description,
    required String enclosure,
    required int feedId,
    required String guid,
    required String imageUrl,
    required String link,
    required int id,
    required double putDate,
    required String source,
    required String title,
  }) = _ArticleModel;

  factory ArticleModel.fromJson(Map<String, Object?> json) => _$ArticleModelFromJson(json);
}
