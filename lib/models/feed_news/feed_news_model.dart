import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:overone/models/article/article_model.dart';

part 'feed_news_model.freezed.dart';

part 'feed_news_model.g.dart';

@freezed
class FeedNewsModel with _$FeedNewsModel {
  factory FeedNewsModel({
    required int feedid,
    required bool hasUpdates,
    required List<ArticleModel> news,
    required String type,
  }) = _FeedNewsModel;

  factory FeedNewsModel.fromJson(Map<String, Object?> json) => _$FeedNewsModelFromJson(json);
}
