import 'package:freezed_annotation/freezed_annotation.dart';

part 'article_actions_model.freezed.dart';

part 'article_actions_model.g.dart';

@freezed
class ArticleActionsModel with _$ArticleActionsModel {
  factory ArticleActionsModel({
    required bool success
  }) = _ArticleActionsModel;

  factory ArticleActionsModel.fromJson(Map<String, Object?> json) => _$ArticleActionsModelFromJson(json);
}
