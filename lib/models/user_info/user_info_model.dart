import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_info_model.freezed.dart';

part 'user_info_model.g.dart';

@freezed
class UserInfoModel with _$UserInfoModel {
  factory UserInfoModel({
    required int id,
    required String login,
    required String avatar,
  }) = _UserInfoModel;

  factory UserInfoModel.fromJson(Map<String, Object?> json) => _$UserInfoModelFromJson(json);
}
