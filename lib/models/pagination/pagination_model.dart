import 'package:freezed_annotation/freezed_annotation.dart';

part 'pagination_model.freezed.dart';

part 'pagination_model.g.dart';

@freezed
class PaginationModel with _$PaginationModel {
  const PaginationModel._();

  factory PaginationModel({
    required int limit,
    required int offset,
    required int firstId,
    required int lastId,
    required int sort,
    required int order,
  }) = _PaginationModel;

  factory PaginationModel.fromJson(Map<String, Object?> json) => _$PaginationModelFromJson(json);

  Map<String, dynamic> toJsonWithStringTypes() {
    return toJson().map((key, value) => MapEntry(key, value.toString()));
  }
}
