import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:overone/models/article/article_model.dart';

part 'news_model.freezed.dart';

part 'news_model.g.dart';

@freezed
class NewsModel with _$NewsModel {
  factory NewsModel({
    @Default(false) bool hasUpdates,
    required List<ArticleModel> news,
    required String type,
  }) = _NewsModel;

  factory NewsModel.fromJson(Map<String, Object?> json) => _$NewsModelFromJson(json);
}
