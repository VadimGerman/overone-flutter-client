import 'package:freezed_annotation/freezed_annotation.dart';

part 'feed_model.freezed.dart';

part 'feed_model.g.dart';

@freezed
class FeedModel with _$FeedModel {
  factory FeedModel({
    required int id,
    required String link,
    required String title,
    required String description,
    required String imageUrl,
    required String language,
    required bool isSubscribed,
  }) = _FeedModel;

  factory FeedModel.fromJson(Map<String, Object?> json) => _$FeedModelFromJson(json);
}
