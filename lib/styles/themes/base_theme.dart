import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:overone/styles/app_fonts.dart';

abstract class BaseTheme {
  final ThemeData themeData;

  BaseTheme({
    required Brightness brightness,
  }) : themeData = ThemeData(
    brightness: brightness,
    fontFamily: AppFonts.helveticaNeueCyr,
  );
}