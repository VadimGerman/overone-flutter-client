import 'package:flutter/material.dart';
import 'package:overone/styles/themes/base_theme.dart';

class DarkTheme extends BaseTheme {
  DarkTheme()
      : super(
          brightness: Brightness.dark,
        );
}
