import 'package:flutter/material.dart';
import 'package:overone/styles/themes/base_theme.dart';

class LightTheme extends BaseTheme {
  LightTheme()
      : super(
          brightness: Brightness.light,
        );
}
