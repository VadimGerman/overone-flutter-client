class AppSizes {
  static const double desktopMinWidth = 1366;

  static bool isDesktop(double width) => width >= desktopMinWidth;
}