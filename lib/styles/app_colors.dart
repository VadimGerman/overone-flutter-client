import 'package:flutter/material.dart';

class AppColors {
  static final Color lightScaffoldBackgroundColor = Color(0xFFF9F9F9);
  static final Color darkScaffoldBackgroundColor = Color(0xFF2F2E2E);
  static final Color secondaryAppColor = Color(0xFF5E92F3);
  static final Color secondaryDarkAppColor = Colors.white;
}
