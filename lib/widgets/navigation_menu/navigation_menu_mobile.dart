import 'package:flutter/material.dart';
import 'package:overone/widgets/navigation_menu/components/buttons/navigation_menu_button_profile.dart';
import 'package:overone/widgets/navigation_menu/components/buttons/navigation_menu_button_articles.dart';
import 'package:overone/widgets/navigation_menu/components/buttons/navigation_menu_button_categories.dart';
import 'package:overone/widgets/navigation_menu/components/buttons/navigation_menu_button_favorites.dart';
import 'package:overone/widgets/navigation_menu/components/buttons/navigation_menu_button_folders.dart';

class NavigationMenuMobile extends StatelessWidget {
  const NavigationMenuMobile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFF4F4F4),
      padding: const EdgeInsets.symmetric(
        horizontal: 30,
        vertical: 13,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          NavigationMenuButtonArticles(),
          NavigationMenuButtonFavorites(),
          NavigationMenuButtonCategories(),
          NavigationMenuButtonFolders(),
          NavigationMenuButtonProfile(),
        ],
      ),
    );
  }
}
