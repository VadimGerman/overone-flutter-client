import 'package:flutter/material.dart';

class NavigationMenuButton extends StatelessWidget {
  final Widget icon;
  final String label;
  final VoidCallback? onPressed;
  final bool selected;

  const NavigationMenuButton({
    Key? key,
    required this.icon,
    required this.label,
    this.onPressed,
    this.selected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 60,
      child: Column(
        children: [
          IconButton(
            onPressed: onPressed,
            icon: icon,
            splashRadius: 20,
            color: selected ? Color(0xFF0A93A6) : Color(0xFF808486),
          ),
          if (selected)
            Text(
              label,
              style: TextStyle(
                color: Color(0xFF0A93A6),
                fontSize: 12
              ),
            ),
        ],
      ),
    );
  }
}
