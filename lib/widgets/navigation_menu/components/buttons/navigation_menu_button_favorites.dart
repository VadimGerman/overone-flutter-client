import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:flutter/material.dart';
import 'package:overone/router/app_router.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/navigation_menu/components/navigation_menu_button.dart';

class NavigationMenuButtonFavorites extends StatelessWidget {
  const NavigationMenuButtonFavorites({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NavigationMenuButton(
      icon: Icon(Icons.bookmark_border),
      label: context.l10n.favorites,
      selected: [
        FavoritesRoute.name,
        FavoritesHistoryRoute.name,
        FavoritesBookmarksRoute.name,
      ].contains(context.router.current.name),
      onPressed: () {
        context.router.push(FavoritesRoute());
      },
    );
  }
}
