import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:flutter/material.dart';
import 'package:overone/router/app_router.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/navigation_menu/components/navigation_menu_button.dart';

class NavigationMenuButtonProfile extends StatelessWidget {
  const NavigationMenuButtonProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NavigationMenuButton(
      icon: Icon(Icons.account_circle_outlined),
      label: context.l10n.profile,
      selected: context.router.current.name == ProfileRoute.name,
      onPressed: () {
        context.router.push(ProfileRoute());
      },
    );
  }
}
