import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:flutter/material.dart';
import 'package:overone/bloc/authentication/authentication_bloc.dart';
import 'package:overone/router/app_router.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/navigation_menu/components/navigation_menu_button.dart';
import 'package:provider/src/provider.dart';

class NavigationMenuButtonArticles extends StatelessWidget {
  const NavigationMenuButtonArticles({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NavigationMenuButton(
      icon: Icon(Icons.article),
      label: context.l10n.newsFeed,
      selected: [
        NewsRoute.name,
        NewsMyFeedRoute.name,
        NewsFavoritesRoute.name,
      ].contains(context.router.current.name),
      onPressed: () {
        context.router.push(NewsRoute());
      },
    );
  }
}
