import 'package:flutter/material.dart';
import 'package:overone/widgets/navigation_menu/components/buttons/navigation_menu_button_articles.dart';
import 'package:overone/widgets/navigation_menu/components/buttons/navigation_menu_button_categories.dart';
import 'package:overone/widgets/navigation_menu/components/buttons/navigation_menu_button_favorites.dart';
import 'package:overone/widgets/navigation_menu/components/buttons/navigation_menu_button_folders.dart';

class NavigationMenuDesktop extends StatelessWidget {
  const NavigationMenuDesktop({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 70.0,
        vertical: 20,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        children: [
          NavigationMenuButtonArticles(),
          NavigationMenuButtonFavorites(),
          NavigationMenuButtonCategories(),
          NavigationMenuButtonFolders(),
        ],
      ),
    );
  }
}
