import 'package:flutter/material.dart';

class SearchField extends StatelessWidget {
  const SearchField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          width: 200,
          height: 37,
          child: TextField(
            style: TextStyle(fontSize: 14, height: 16 / 14),
            maxLines: 1,
            decoration: InputDecoration(
              isDense: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0),
                borderSide: BorderSide(
                  color: Color(0xFFEBEBEB),
                ),
              ),
              filled: true,
              fillColor: Color(0xFFEBEBEB),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0),
                borderSide: BorderSide(
                  color: Color(0xFFEBEBEB),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(width: 20),
        Icon(Icons.search),
      ],
    );
  }
}
