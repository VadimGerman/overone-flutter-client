import 'package:flutter/material.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/headers/desktop_header.dart';
import 'package:overone/widgets/headers/mobile_header.dart';
import 'package:overone/widgets/navigation_menu/navigation_menu_mobile.dart';

class MobileScreenWrapper extends StatelessWidget {
  final Widget child;
  final Widget? headerChild;

  const MobileScreenWrapper({
    Key? key,
    required this.child,
    this.headerChild,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          MobileHeader(
            child: headerChild,
          ),
          Expanded(child: child),
          NavigationMenuMobile(),
        ],
      ),
    );
  }
}
