import 'package:flutter/material.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/headers/desktop_header.dart';

class DesktopScreenWrapper extends StatelessWidget {
  final Widget child;
  final Widget? headerChild;

  const DesktopScreenWrapper({
    Key? key,
    required this.child,
    this.headerChild,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = context.mediaQuery;
    return Scaffold(
      body: Column(
        children: [
          DesktopHeader(
            child: headerChild,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: mediaQuery.size.width * 0.1),
              child: child,
            ),
          ),
        ],
      ),
    );
  }
}
