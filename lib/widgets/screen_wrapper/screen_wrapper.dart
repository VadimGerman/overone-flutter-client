import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/styles/app_sizes.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/screen_wrapper/components/desktop_screen_wrapper.dart';
import 'package:overone/widgets/screen_wrapper/components/mobile_screen_wrapper.dart';

class ScreenWrapper extends StatelessWidget {
  final Widget child;
  final Widget? headerChild;

  const ScreenWrapper({
    Key? key,
    required this.child,
    this.headerChild,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: context,
      child: SafeArea(
        child: _buildPlatformWrapper(context),
      ),
    );
  }

  Widget _buildPlatformWrapper(BuildContext context) {
    return AppSizes.isDesktop(context.mediaQuery.size.width)
        ? DesktopScreenWrapper(
      child: child,
      headerChild: headerChild,
    )
        : MobileScreenWrapper(
      child: child,
      headerChild: headerChild,
    );
  }
}
