import 'package:flutter/material.dart';

class DropMenuButtonItem {
  final Widget child;
  final VoidCallback onPressed;

  DropMenuButtonItem({
    required this.child,
    required this.onPressed,
  });
}

class DropMenuButton extends StatelessWidget {
  final List<DropMenuButtonItem> items;
  final Color iconColor;
  final double iconSize;

  const DropMenuButton({
    Key? key,
    required this.items,
    required this.iconColor,
    required this.iconSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: TextStyle(
        fontSize: 12,
      ),
      child: SizedBox(
        width: iconSize * 2,
        height: iconSize * 2,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: Material(
            color: Colors.transparent,
            child: PopupMenuButton<DropMenuButtonItem>(
              itemBuilder: (context) => [
                for (final item in items)
                  PopupMenuItem(
                    value: item,
                    child: item.child,
                  ),
              ],
              onSelected: (DropMenuButtonItem value) {
                value.onPressed();
              },
              child: Icon(
                Icons.more_vert,
                size: iconSize,
                color: iconColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
