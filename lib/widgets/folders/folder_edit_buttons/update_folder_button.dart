import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/models/folder/folder_model.dart';
import 'package:overone/widgets/folders/dialogs/update_folder_dialog.dart';

class UpdateFolderButton extends StatelessWidget {
  final FolderModel folder;

  const UpdateFolderButton({
    Key? key,
    required this.folder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: 0.7,
      child: IconButton(
        icon: Icon(
          Icons.edit,
          color: Color(0xFF999999),
        ),
        onPressed: () {
          final rootContext = context.read<BuildContext>();
          showDialog(
            context: rootContext,
            builder: (context) {
              return UpdateFolderDialog(
                rootContext: rootContext,
                folder: folder,
              );
            },
          );
        },
      ),
    );
  }
}
