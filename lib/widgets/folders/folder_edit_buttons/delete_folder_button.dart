import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/api/folders_api.dart';
import 'package:overone/bloc/folders/folders_bloc.dart';
import 'package:overone/utils/build_context_extension.dart';

class DeleteFolderButton extends StatelessWidget {
  final int folderId;

  const DeleteFolderButton({
    Key? key,
    required this.folderId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: 0.7,
      child: IconButton(
        icon: Icon(
          Icons.delete,
          color: Color(0xFF999999),
        ),
        onPressed: () async {
          await FoldersApi.deleteFolder(authentication: context.authentication, folderId: folderId);
          context.read<FoldersBloc>().add(FoldersEvent.getFolders());
        },
      ),
    );
  }
}
