import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/folders/folders_bloc.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/widgets/folders/dialogs/unsubscribe_feed_dialog.dart';

class UnsubscribeFeedButton extends StatelessWidget {
  final FeedModel feed;
  final int? folderId;
  final VoidCallback? onUnsubscribe;

  const UnsubscribeFeedButton({
    Key? key,
    required this.feed,
    this.folderId,
    this.onUnsubscribe,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final providersContext = context;
    return Transform.scale(
      scale: 0.7,
      child: IconButton(
        icon: Icon(
          Icons.delete,
          color: Color(0xFF999999),
        ),
        onPressed: () async {
          final rootContext = context.read<BuildContext>();
          showDialog(
            context: rootContext,
            builder: (context) => UnsubscribeFeedDialog(
              rootContext: rootContext,
              feed: feed,
              folderId: folderId,
              onAllUnsubscribe: () {
                providersContext.read<FoldersBloc>().add(FoldersEvent.feedUnsubscribeAll(feed.id));
                onUnsubscribe?.call();
              },
              onFolderUnsubscribe: () {
                providersContext.read<FoldersBloc>().add(FoldersEvent.feedUnsubscribeFolder(feed.id, folderId!));
                onUnsubscribe?.call();
              },
            ),
          );
        },
      ),
    );
  }
}
