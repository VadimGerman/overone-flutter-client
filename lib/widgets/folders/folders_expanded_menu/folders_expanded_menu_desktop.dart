import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/bloc/articles/implemantations/articles_folders_bloc.dart';
import 'package:overone/bloc/folders/folders_bloc.dart';
import 'package:overone/models/folder/folder_model.dart';
import 'package:overone/widgets/common/expanded_menu/components/expanded_menu_item_params.dart';
import 'package:overone/widgets/common/expanded_menu/expanded_menu.dart';
import 'package:overone/widgets/folders/dialogs/create_folder_dialog.dart';
import 'package:overone/widgets/folders/menu_items/add_folder_item.dart';
import 'package:overone/widgets/folders/menu_items/all_feeds_folder_item.dart';
import 'package:overone/widgets/folders/menu_items/feed_item.dart';
import 'package:overone/widgets/folders/menu_items/folder_item.dart';

class FoldersExpandedMenuDesktop extends StatelessWidget {
  final void Function(BuildContext context) allFeedSelected;
  final void Function(BuildContext context, FolderModel folder) folderSelected;

  const FoldersExpandedMenuDesktop({
    Key? key,
    required this.allFeedSelected,
    required this.folderSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FoldersBloc, FoldersState>(
      builder: (context, state) {
        return ExpandedMenu(
          items: [
            ExpandedMenuItemParams(
              onPressed: () {
                allFeedSelected(context);
              },
              hasChildren: state.allFeeds.isNotEmpty,
              child: AllFeedsFolderMenuItem(
                feedsCount: state.allFeeds.length,
              ),
              children: [
                for (final feed in state.allFeeds)
                  ExpandedMenuItemParams(
                    child: FeedMenuItem(
                      feed: feed,
                      onUnsubscribe: () {
                        context.read<ArticlesFoldersBloc>().add(ArticlesEvent.refresh());
                      },
                    ),
                  ),
              ],
            ),
            ExpandedMenuItemParams(
              child: AddFolderMenuItem(),
              onPressed: () {
                final rootContext = context.read<BuildContext>();
                showDialog(
                  context: rootContext,
                  builder: (context) {
                    return CreateFolderDialog(
                      rootContext: rootContext,
                      onSuccess: (folder) {
                        context.read<FoldersBloc>().add(FoldersEvent.getFolders());
                      },
                    );
                  },
                );
              }
            ),
            for (final folder in state.folders)
              ExpandedMenuItemParams(
                onPressed: () {
                  context.read<FoldersBloc>().add(FoldersEvent.getFolderFeeds(folder.id));
                  folderSelected(context, folder);
                },
                hasChildren: folder.feedsCount > 0,
                child: FolderMenuItem(
                  folder: folder,
                ),
                children: [
                  if (state.folderFeeds[folder.id] != null)
                    for (final folderFeed in state.folderFeeds[folder.id]!)
                      ExpandedMenuItemParams(
                        child: FeedMenuItem(
                          feed: folderFeed,
                          folderId: folder.id,
                          onUnsubscribe: () {
                            context.read<ArticlesFoldersBloc>().add(ArticlesEvent.refresh());
                          },
                        ),
                      ),
                ],
              ),
          ],
        );
      },
    );
  }
}
