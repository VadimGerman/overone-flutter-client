import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/create_folder_form/create_folder_form_bloc.dart';
import 'package:overone/bloc/folders/folders_bloc.dart';
import 'package:overone/models/folder/folder_model.dart';
import 'package:overone/utils/build_context_extension.dart';

class CreateFolderDialog extends StatelessWidget {
  final BuildContext rootContext;
  final void Function(FolderModel folder)? onSuccess;

  const CreateFolderDialog({
    Key? key,
    required this.rootContext,
    this.onSuccess,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bloc(
      onSuccess: onSuccess,
      rootContext: rootContext,
      child: AlertDialog(
        title: Text(
          context.l10n.createFolder,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
        ),
        content: Container(
          width: 320,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 26,
              ),
              _TitleField(),
              // _IconNameField(),
              // _IsPublicField(),
              _FormError(),
            ],
          ),
        ),
        actions: [
          _SubmitButton(),
        ],
      ),
    );
  }
}

class _Bloc extends StatelessWidget {
  final BuildContext rootContext;
  final Widget child;
  final void Function(FolderModel folder)? onSuccess;

  const _Bloc({
    required this.rootContext,
    required this.child,
    this.onSuccess,
  });

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => CreateFolderFormBloc(
            authentication: rootContext.authentication,
            l10n: rootContext.l10n,
          ),
        ),
      ],
      child: BlocListener<CreateFolderFormBloc, CreateFolderFormState>(
        listenWhen: (p, n) => p.newFolder != n.newFolder,
        listener: (context, state) {
          context.router.pop();
          onSuccess?.call(state.newFolder!);
        },
        child: child,
      ),
    );
  }
}

class _TitleField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFolderFormBloc, CreateFolderFormState>(
      buildWhen: (p, n) => p.titleError != n.titleError,
      builder: (context, state) {
        return TextField(
          onChanged: (value) {
            context.read<CreateFolderFormBloc>().add(CreateFolderFormEvent.changeTitle(value));
          },
          decoration: InputDecoration(
            labelText: context.l10n.title,
            border: OutlineInputBorder(),
            errorText: state.titleError,
            helperText: '',
          ),
        );
      },
    );
  }
}

class _IconNameField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: (value) {
        context.read<CreateFolderFormBloc>().add(CreateFolderFormEvent.changeIconName(value));
      },
      decoration: InputDecoration(
        labelText: context.l10n.iconName,
        border: OutlineInputBorder(),
        helperText: '',
      ),
    );
  }
}

class _IsPublicField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFolderFormBloc, CreateFolderFormState>(
      buildWhen: (p, n) => p.isPublic != n.isPublic,
      builder: (context, state) {
        return Row(
          children: [
            Text(context.l10n.isPublicFolder),
            Checkbox(
              value: state.isPublic,
              onChanged: (value) {
                context.read<CreateFolderFormBloc>().add(CreateFolderFormEvent.changeIsPublic(value!));
              },
            ),
          ],
        );
      },
    );
  }
}

class _FormError extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFolderFormBloc, CreateFolderFormState>(
      buildWhen: (p, n) => p.formError != n.formError,
      builder: (context, state) {
        return Text(
          state.formError ?? '',
          style: TextStyle(color: Colors.red),
        );
      },
    );
  }
}

class _SubmitButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(context.l10n.save),
      onPressed: () {
        context.read<CreateFolderFormBloc>().add(CreateFolderFormEvent.submit());
      },
    );
  }
}
