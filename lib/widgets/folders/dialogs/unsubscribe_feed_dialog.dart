import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/models/folder/folder_model.dart';
import 'package:overone/utils/build_context_extension.dart';

class UnsubscribeFeedDialog extends StatelessWidget {
  final BuildContext rootContext;
  final FeedModel feed;
  final int? folderId;
  final VoidCallback? onAllUnsubscribe;
  final void Function()? onFolderUnsubscribe;

  const UnsubscribeFeedDialog({
    Key? key,
    required this.rootContext,
    required this.feed,
    this.folderId,
    required this.onAllUnsubscribe,
    required this.onFolderUnsubscribe,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        folderId != null ? context.l10n.feedUnsubscribeTitle : context.l10n.feedUnsubscribeTitleFromAllFeeds,
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
      ),
      content: Container(
        width: 320,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TextButton(
              child: Text(context.l10n.cancel.toUpperCase()),
              onPressed: () {
                context.router.pop();
              },
            ),
            if (folderId != null)
              TextButton(
                child: Text(context.l10n.ok.toUpperCase()),
                onPressed: () async {
                  onFolderUnsubscribe?.call();
                  context.router.pop();
                },
              ),
            TextButton(
              child: Text(context.l10n.unsubscribeCompletely),
              onPressed: () async {
                onAllUnsubscribe?.call();
                context.router.pop();
              },
            ),
          ],
        ),
      ),
    );
  }
}
