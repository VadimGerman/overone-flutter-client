import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/create_folder_form/create_folder_form_bloc.dart';
import 'package:overone/bloc/folders/folders_bloc.dart';
import 'package:overone/bloc/update_folder_form/update_folder_form_bloc.dart';
import 'package:overone/models/folder/folder_model.dart';
import 'package:overone/utils/build_context_extension.dart';

class UpdateFolderDialog extends StatelessWidget {
  final BuildContext rootContext;
  final FolderModel folder;

  const UpdateFolderDialog({
    Key? key,
    required this.rootContext,
    required this.folder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bloc(
      rootContext: rootContext,
      folder: folder,
      child: AlertDialog(
        title: Text(
          context.l10n.updateFolder,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
        ),
        content: Container(
          width: 320,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 26,
              ),
              _TitleField(),
              // _IconNameField(),
              // _IsPublicField(),
              _FormError(),
            ],
          ),
        ),
        actions: [
          _ResetButton(),
          _SubmitButton(),
        ],
      ),
    );
  }
}

class _Bloc extends StatelessWidget {
  final BuildContext rootContext;
  final FolderModel folder;
  final Widget child;

  const _Bloc({
    Key? key,
    required this.rootContext,
    required this.folder,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => UpdateFolderFormBloc(
            authentication: rootContext.authentication,
            l10n: rootContext.l10n,
            folder: folder,
          ),
        ),
        BlocProvider.value(
          value: rootContext.read<FoldersBloc>(),
        ),
      ],
      child: BlocListener<UpdateFolderFormBloc, UpdateFolderFormState>(
        listenWhen: (p, n) => p.newFolder != n.newFolder,
        listener: (context, state) {
          context.read<FoldersBloc>().add(FoldersEvent.getFolders());
          context.router.pop();
        },
        child: child,
      ),
    );
  }
}

class _TitleField extends StatefulWidget {
  @override
  State<_TitleField> createState() => _TitleFieldState();
}

class _TitleFieldState extends State<_TitleField> {
  late TextEditingController _textController;

  @override
  void initState() {
    _textController = TextEditingController(
      text: context.read<UpdateFolderFormBloc>().state.title,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<UpdateFolderFormBloc, UpdateFolderFormState>(
      listenWhen: (p, n) => p.title != n.title,
      listener: (context, state) {
        _textController.value = TextEditingValue(
          text: state.title,
          selection: TextSelection.fromPosition(
            TextPosition(offset: state.title.length),
          ),
        );
      },
      child: BlocBuilder<UpdateFolderFormBloc, UpdateFolderFormState>(
        buildWhen: (p, n) => p.titleError != n.titleError,
        builder: (context, state) {
          return TextField(
            controller: _textController,
            onChanged: (value) {
              context.read<UpdateFolderFormBloc>().add(UpdateFolderFormEvent.changeTitle(value));
            },
            decoration: InputDecoration(
              labelText: context.l10n.title,
              border: OutlineInputBorder(),
              errorText: state.titleError,
              helperText: '',
            ),
          );
        },
      ),
    );
  }
}

class _IconNameField extends StatefulWidget {
  @override
  State<_IconNameField> createState() => _IconNameFieldState();
}

class _IconNameFieldState extends State<_IconNameField> {
  late TextEditingController _textController;

  @override
  void initState() {
    _textController = TextEditingController(
      text: context.read<UpdateFolderFormBloc>().state.iconName,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<UpdateFolderFormBloc, UpdateFolderFormState>(
      listenWhen: (p, n) => p.iconName != n.iconName,
      listener: (context, state) {
        _textController.value = TextEditingValue(
          text: state.iconName,
          selection: TextSelection.fromPosition(
            TextPosition(offset: state.iconName.length),
          ),
        );
      },
      child: TextField(
        controller: _textController,
        onChanged: (value) {
          context.read<UpdateFolderFormBloc>().add(UpdateFolderFormEvent.changeIconName(value));
        },
        decoration: InputDecoration(
          labelText: context.l10n.iconName,
          border: OutlineInputBorder(),
          helperText: '',
        ),
      ),
    );
  }
}

class _IsPublicField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UpdateFolderFormBloc, UpdateFolderFormState>(
      buildWhen: (p, n) => p.isPublic != n.isPublic,
      builder: (context, state) {
        return Row(
          children: [
            Text(context.l10n.isPublicFolder),
            Checkbox(
              value: state.isPublic,
              onChanged: (value) {
                context.read<UpdateFolderFormBloc>().add(UpdateFolderFormEvent.changeIsPublic(value!));
              },
            ),
          ],
        );
      },
    );
  }
}

class _FormError extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UpdateFolderFormBloc, UpdateFolderFormState>(
      buildWhen: (p, n) => p.formError != n.formError,
      builder: (context, state) {
        return Text(
          state.formError ?? '',
          style: TextStyle(color: Colors.red),
        );
      },
    );
  }
}

class _ResetButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(context.l10n.reset),
      onPressed: () {
        context.read<UpdateFolderFormBloc>().add(UpdateFolderFormEvent.reset());
      },
    );
  }
}

class _SubmitButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(context.l10n.save),
      onPressed: () {
        context.read<UpdateFolderFormBloc>().add(UpdateFolderFormEvent.submit());
      },
    );
  }
}
