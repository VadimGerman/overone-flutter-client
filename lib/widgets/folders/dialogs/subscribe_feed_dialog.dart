import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/api/folders_api.dart';
import 'package:overone/bloc/create_folder_form/create_folder_form_bloc.dart';
import 'package:overone/bloc/folders/folders_bloc.dart';
import 'package:overone/bloc/multi_select/multi_select_bloc.dart';
import 'package:overone/bloc/update_folder_form/update_folder_form_bloc.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/models/folder/folder_model.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/folders/dialogs/create_folder_dialog.dart';

class SubscribeFeedDialog extends StatelessWidget {
  final BuildContext rootContext;
  final FeedModel feed;
  final FolderModel? folder;
  final void Function(List<FolderModel> folders)? onSubmitted;

  const SubscribeFeedDialog({
    Key? key,
    required this.rootContext,
    required this.feed,
    this.folder,
    this.onSubmitted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          lazy: false,
          create: (context) => FoldersBloc(
            authentication: context.authentication,
            initLoadFolders: true,
          ),
        ),
        BlocProvider(
          create: (context) => MultiSelectBloc<FolderModel>(),
        ),
      ],
      child: Builder(
        builder: (context) {
          final providersContext = context;
          return AlertDialog(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  context.l10n.selectFolder,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
                TextButton(
                  child: Text(context.l10n.createFolder),
                  onPressed: () {
                    showDialog(
                      context: rootContext,
                      builder: (context) {
                        return CreateFolderDialog(
                          rootContext: rootContext,
                          onSuccess: (folder) {
                            providersContext.read<FoldersBloc>().add(FoldersEvent.getFolders());
                            providersContext.read<MultiSelectBloc<FolderModel>>().add(MultiSelectEvent.toggle(folder));
                          },
                        );
                      },
                    );
                  },
                )
              ],
            ),
            content: Container(
              width: 320,
              child: BlocBuilder<FoldersBloc, FoldersState>(
                buildWhen: (p, n) => p.folders != n.folders,
                builder: (context, state) {
                  return Column(
                    children: [
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Icon(Icons.folder_open),
                                  SizedBox(width: 10),
                                  Text(context.l10n.allSubscriptions),
                                  Expanded(
                                    child: SizedBox(),
                                  ),
                                  Icon(
                                    Icons.check,
                                    color: Colors.grey,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              for (final folder in state.folders) ...[
                                MouseRegion(
                                  cursor: SystemMouseCursors.click,
                                  child: GestureDetector(
                                    behavior: HitTestBehavior.opaque,
                                    onTap: () {
                                      context.read<MultiSelectBloc<FolderModel>>().add(MultiSelectEvent.toggle(folder));
                                    },
                                    child: BlocBuilder<MultiSelectBloc<FolderModel>, MultiSelectState>(
                                      builder: (context, state) {
                                        return Row(
                                          children: [
                                            Icon(Icons.folder),
                                            SizedBox(width: 10),
                                            Text(folder.title),
                                            Expanded(
                                              child: SizedBox(),
                                            ),
                                            if (state.items.contains(folder))
                                              Icon(
                                                Icons.check,
                                                color: Color(0xFF0A93A6),
                                              ),
                                          ],
                                        );
                                      },
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                              ],
                            ],
                          ),
                        ),
                      ),
                      BlocBuilder<MultiSelectBloc<FolderModel>, MultiSelectState<FolderModel>>(
                        builder: (context, state) {
                          return ElevatedButton(
                            child: Text(context.l10n.ready),
                            onPressed: () async {
                              await FoldersApi.subscribeFeed(
                                authentication: context.authentication,
                                doSubscribe: true,
                                feedId: feed.id,
                                foldersIds: state.items.map((e) => e.id).toList(),
                              );
                              context.router.pop();
                              onSubmitted?.call(state.items);
                            },
                          );
                        },
                      )
                    ],
                  );
                },
              ),
            ),
          );
        },
      ),
    );
  }
}
