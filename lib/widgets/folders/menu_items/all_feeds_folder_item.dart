import 'package:flutter/material.dart';
import 'package:overone/utils/build_context_extension.dart';

class AllFeedsFolderMenuItem extends StatelessWidget {
  final int feedsCount;

  final greyColor = Color(0xFF999999);

  AllFeedsFolderMenuItem({
    Key? key,
    required this.feedsCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(
          Icons.folder_open,
          color: greyColor,
        ),
        SizedBox(width: 18),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(context.l10n.allSubscriptions),
            SizedBox(
              height: 8,
            ),
            Text(
              context.l10n.feedsCount(feedsCount),
              style: TextStyle(
                color: greyColor,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
