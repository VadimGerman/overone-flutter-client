import 'package:flutter/material.dart';
import 'package:overone/utils/build_context_extension.dart';

class AddFolderMenuItem extends StatelessWidget {
  AddFolderMenuItem({
    Key? key,
  }) : super(key: key);

  final greenColor = Color(0xFF0A93A6);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.create_new_folder,
          color: greenColor,
        ),
        SizedBox(width: 18),
        Text(context.l10n.createFolder),
      ],
    );
  }
}
