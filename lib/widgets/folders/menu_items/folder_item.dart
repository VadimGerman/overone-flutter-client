import 'package:flutter/material.dart';
import 'package:overone/models/folder/folder_model.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/folders/folder_edit_buttons/delete_folder_button.dart';
import 'package:overone/widgets/folders/folder_edit_buttons/update_folder_button.dart';

class FolderMenuItem extends StatelessWidget {
  final FolderModel folder;

  final greyColor = Color(0xFF999999);

  FolderMenuItem({
    Key? key,
    required this.folder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Row(
            children: [
              Icon(
                Icons.folder,
                color: greyColor,
              ),
              SizedBox(width: 18),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      folder.title,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      context.l10n.feedsCount(folder.feedsCount),
                      style: TextStyle(
                        color: greyColor,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        Row(
          children: [
            UpdateFolderButton(
              folder: folder,
            ),
            DeleteFolderButton(
              folderId: folder.id,
            ),
          ],
        ),
      ],
    );
  }
}
