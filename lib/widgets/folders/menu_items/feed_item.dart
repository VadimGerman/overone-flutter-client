import 'package:flutter/material.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/widgets/folders/folder_edit_buttons/unsubscribe_feed_button.dart';

class FeedMenuItem extends StatelessWidget {
  final FeedModel feed;
  final int? folderId;
  final VoidCallback? onUnsubscribe;

  final greyColor = Color(0xFF999999);

  FeedMenuItem({
    Key? key,
    required this.feed,
    this.folderId,
    this.onUnsubscribe,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          feed.title,
          style: TextStyle(
            color: greyColor,
          ),
        ),
        Expanded(child: SizedBox()),
        UnsubscribeFeedButton(
          feed: feed,
          folderId: folderId,
          onUnsubscribe: onUnsubscribe,
        ),
      ],
    );
  }
}
