import 'package:flutter/material.dart';
import 'package:overone/widgets/common/expanded_menu/components/expanded_menu_item.dart';
import 'package:overone/widgets/common/expanded_menu/components/expanded_menu_item_params.dart';

class ExpandedMenu extends StatelessWidget {
  final List<ExpandedMenuItemParams> items;

  const ExpandedMenu({
    super.key,
    required this.items,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (final item in items)
            ExpandedMenuItem(
              item: item,
            ),
        ],
      ),
    );
  }
}
