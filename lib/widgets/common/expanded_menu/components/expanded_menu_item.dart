import 'package:flutter/material.dart';
import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import 'package:overone/widgets/common/expanded_menu/components/expanded_menu_item_params.dart';

class ExpandedMenuItem extends StatefulWidget {
  final ExpandedMenuItemParams item;

  const ExpandedMenuItem({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  State<ExpandedMenuItem> createState() => _ExpandedMenuItemState();
}

class _ExpandedMenuItemState extends State<ExpandedMenuItem> {
  late ExpandedTileController _controller;

  @override
  void initState() {
    _controller = ExpandedTileController();
    _controller.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final item = widget.item;
    final style = item.style;
    final backgroundColor = _controller.isExpanded ? style.backgroundColorSelected : style.backgroundColor;
    final textColor = _controller.isExpanded ? style.textColorSelected : style.textColor;

    if (!item.hasChildren) {
      return Column(
        children: [
          InkWell(
            onTap: () {
              if (item.onPressed != null) {
                item.onPressed!();
              }
            },
            child: MouseRegion(
              cursor: SystemMouseCursors.click,
              hitTestBehavior: HitTestBehavior.opaque,
              child: Container(
                color: backgroundColor,
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: style.verticalPadding + 4.5, horizontal: 14.5).copyWith(left: 22),
                child: DefaultTextStyle(
                  style: TextStyle(color: textColor),
                  child: Row(
                    children: [
                      Expanded(
                        child: item.child,
                      ),
                      SizedBox(
                        width: 40,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 6,
          )
        ],
      );
    } else {
      return ExpandedTile(
        onTap: item.onPressed,
        theme: ExpandedTileThemeData(
          headerColor: backgroundColor,
          headerPadding: EdgeInsets.symmetric(
            vertical: style.verticalPadding,
            horizontal: 10,
          ),
          headerRadius: 0,
          contentRadius: 0,
          contentPadding: const EdgeInsets.all(0),
          contentBackgroundColor: Colors.transparent,
        ),
        trailing: Icon(
          Icons.chevron_right,
          color: textColor,
        ),
        title: DefaultTextStyle(
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: textColor,
          ),
          child: widget.item.child,
        ),
        controller: _controller,
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for (final subItem in item.children)
              ExpandedMenuItem(
                item: subItem,
              ),
          ],
        ),
      );
    }
  }
}
