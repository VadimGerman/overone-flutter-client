import 'package:flutter/material.dart';

class ExpandedMenuItemParams {
  final Widget child;
  final List<ExpandedMenuItemParams> children;
  final bool hasChildren;
  final ExpandedMenuItemStyle style;
  final VoidCallback? onPressed;

  ExpandedMenuItemParams({
    required this.child,
    this.style = const ExpandedMenuItemStyle(),
    this.children = const[],
    this.hasChildren = false,
    this.onPressed,
  });
}

class ExpandedMenuItemStyle {
  final Color backgroundColor;
  final Color backgroundColorSelected;
  final Color textColor;
  final Color textColorSelected;
  final double verticalPadding;

  const ExpandedMenuItemStyle({
    this.backgroundColor = Colors.transparent,
    this.backgroundColorSelected = Colors.transparent,
    this.textColor = Colors.black,
    this.textColorSelected = Colors.black,
    this.verticalPadding = 10,
  });
}
