import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/models/category/category_model.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/buttons/clickable.dart';

class FeedsListItem extends StatelessWidget {
  final FeedModel feed;
  final void Function(FeedModel category) onPressed;
  final void Function(FeedModel category) onAdd;

  const FeedsListItem({
    Key? key,
    required this.feed,
    required this.onPressed,
    required this.onAdd,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: this,
      child: _FeedLink(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _FeedTitle(),
                    const SizedBox(height: 4),
                    _SubTitle(),
                  ],
                ),
                _FeedButton(),
              ],
            ),
            const SizedBox(height: 8),
            Container(
              color: Color(0xFF808486),
              height: 1,
            ),
          ],
        ),
      ),
    );
  }
}

class _FeedLink extends StatelessWidget {
  final Widget child;

  const _FeedLink({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final props = context.read<FeedsListItem>();
    return Clickable(
      onPressed: () {
        props.onPressed(props.feed);
      },
      child: child,
    );
  }
}

class _FeedTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final props = context.read<FeedsListItem>();
    return Text(
      props.feed.title,
      style: TextStyle(
        fontWeight: FontWeight.w600,
        fontSize: 14,
        height: 18 / 14,
        color: Color(0xFF151515),
      ),
    );
  }
}

class _SubTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final props = context.read<FeedsListItem>();
    return Text(
      'Прогнозы валютного рынка',
      // props.feed.description,
      style: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 12,
        height: 18 / 12,
        color: Color(0xFF808486),
      ),
    );
  }
}

class _FeedButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final props = context.read<FeedsListItem>();
    return IconButton(
      splashRadius: 20,
      onPressed: () {

      },
      icon: props.feed.isSubscribed
          ? Icon(
              Icons.check,
              color: Color(0xFF0A93A6),
            )
          : Icon(Icons.add),
    );
  }
}
