import 'package:flutter/material.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/widgets/feeds/feeds_list/components/feeds_list_item.dart';

class FeedsList extends StatelessWidget {
  final List<FeedModel>? feeds;
  final void Function(FeedModel feed) onPressed;
  final void Function(FeedModel feed) onAdd;

  const FeedsList({
    Key? key,
    required this.feeds,
    required this.onPressed,
    required this.onAdd,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (feeds == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return ListView.builder(
        itemCount: feeds?.length ?? 0,
        itemBuilder: (context, index) {
          return FeedsListItem(
            feed: feeds![index],
            onPressed: onPressed,
            onAdd: onAdd,
          );
        },
      );
    }
  }
}
