import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/api/folders_api.dart';
import 'package:overone/bloc/categories/categories_bloc.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/folders/dialogs/subscribe_feed_dialog.dart';

class CategoriesExpandedMenuFeedItem extends StatelessWidget {
  final FeedModel feed;
  final int parentCategoryId;

  const CategoriesExpandedMenuFeedItem({
    Key? key,
    required this.feed,
    required this.parentCategoryId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final providersContext = context;
    return Row(
      children: [
        Expanded(
          child: Text(
            feed.title,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        if (feed.isSubscribed)
          Transform.scale(
            scale: 0.7,
            child: IconButton(
              splashRadius: 1,
              icon: Icon(
                Icons.check,
                color: Color(0xFF0A93A6),
              ),
              onPressed: () async {
                await FoldersApi.subscribeFeed(
                  authentication: context.authentication,
                  doSubscribe: false,
                  feedId: feed.id,
                  foldersIds: [],
                );
                context.read<CategoriesBloc>().add(CategoriesEvent.fetchFeeds(parentCategoryId));
              },
            ),
          ),
        if(!feed.isSubscribed)
          Transform.scale(
            scale: 0.7,
            child: IconButton(
              splashRadius: 1,
              icon: Icon(
                Icons.add,
                color: Color(0xFF999999),
              ),
              onPressed: () {
                final rootContext = context.read<BuildContext>();
                showDialog(
                  context: rootContext,
                  builder: (context) {
                    return SubscribeFeedDialog(
                      rootContext: rootContext,
                      feed: feed,
                      onSubmitted: (folders) {
                        providersContext.read<CategoriesBloc>().add(CategoriesEvent.fetchFeeds(parentCategoryId));
                      },
                    );
                  },
                );
              },
            ),
          ),
      ],
    );
  }
}
