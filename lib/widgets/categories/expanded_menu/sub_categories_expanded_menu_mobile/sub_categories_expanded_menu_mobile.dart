import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/categories/categories_bloc.dart';
import 'package:overone/models/category/category_model.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/widgets/categories/expanded_menu/expanded_menu_items/feed_item.dart';
import 'package:overone/widgets/common/expanded_menu/components/expanded_menu_item_params.dart';
import 'package:overone/widgets/common/expanded_menu/expanded_menu.dart';

class SubCategoriesExpandedMenuMobile extends StatelessWidget {
  final int categoryId;
  final void Function(BuildContext context, CategoryModel subCategory, FeedModel feed) feedSelected;
  const SubCategoriesExpandedMenuMobile({
    Key? key,
    required this.feedSelected,
    required this.categoryId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoriesBloc, CategoriesState>(
      builder: (context, state) {
        return ExpandedMenu(
          items: [
            if (state.categories != null)
              for (final subCategory in state.categories!)
                ExpandedMenuItemParams(
                  hasChildren: subCategory.feedsCount > 0,
                  onPressed: () {
                    context.read<CategoriesBloc>().add(CategoriesEvent.fetchFeeds(subCategory.id));
                  },
                  child: Text(subCategory.title),
                  style: ExpandedMenuItemStyle(
                    textColor: Color(0xFF999999),
                    textColorSelected: Color(0xFF0CAAC0),
                  ),
                  children: [
                    if (state.feeds?[subCategory.id] != null)
                      for (final feed in state.feeds![subCategory.id]!)
                        ExpandedMenuItemParams(
                          onPressed: () {
                            feedSelected(context, subCategory, feed);
                          },
                          child: CategoriesExpandedMenuFeedItem(
                            feed: feed,
                            parentCategoryId: subCategory.id,
                          ),
                          style: ExpandedMenuItemStyle(
                            textColor: Color(0xFF999999),
                            textColorSelected: Color(0xFF0CAAC0),
                            verticalPadding: 0.5,
                          ),
                        ),
                  ],
                ),
          ],
        );
      },
    );
  }
}
