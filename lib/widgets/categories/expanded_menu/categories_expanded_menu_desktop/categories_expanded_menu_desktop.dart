import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/categories/categories_bloc.dart';
import 'package:overone/models/feed/feed_model.dart';
import 'package:overone/widgets/categories/expanded_menu/expanded_menu_items/feed_item.dart';
import 'package:overone/widgets/common/expanded_menu/components/expanded_menu_item_params.dart';
import 'package:overone/widgets/common/expanded_menu/expanded_menu.dart';

class CategoriesExpandedMenuDesktop extends StatelessWidget {
  final void Function(BuildContext context, FeedModel feed) feedSelected;
  const CategoriesExpandedMenuDesktop({
    Key? key,
    required this.feedSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoriesBloc, CategoriesState>(
      builder: (context, state) {
        return ExpandedMenu(
          items: [
            if (state.categories != null)
              for (final category in state.categories!)
                ExpandedMenuItemParams(
                  onPressed: () {
                    context.read<CategoriesBloc>().add(CategoriesEvent.fetchSubCategories(category.id));
                  },
                  hasChildren: category.subcategoriesCount > 0,
                  child: Text(category.title),
                  style: ExpandedMenuItemStyle(
                    backgroundColorSelected: Color(0xFF0CAAC0),
                    textColorSelected: Colors.white,
                  ),
                  children: [
                    if (state.subCategories?[category.id] != null)
                      for (final subCategory in state.subCategories![category.id]!)
                        ExpandedMenuItemParams(
                          hasChildren: subCategory.feedsCount > 0,
                          onPressed: () {
                            context.read<CategoriesBloc>().add(CategoriesEvent.fetchFeeds(subCategory.id));
                          },
                          child: Text(subCategory.title),
                          style: ExpandedMenuItemStyle(
                            textColor: Color(0xFF999999),
                            textColorSelected: Color(0xFF0CAAC0),
                          ),
                          children: [
                            if (state.feeds?[subCategory.id] != null)
                              for (final feed in state.feeds![subCategory.id]!)
                                ExpandedMenuItemParams(
                                  onPressed: () {
                                    feedSelected(context, feed);
                                  },
                                  child: CategoriesExpandedMenuFeedItem(
                                    feed: feed,
                                    parentCategoryId: subCategory.id,
                                  ),
                                  style: ExpandedMenuItemStyle(
                                    textColor: Color(0xFF999999),
                                    textColorSelected: Color(0xFF0CAAC0),
                                    verticalPadding: 0.5,
                                  ),
                                ),
                          ],
                        ),
                  ],
                ),
          ],
        );
      },
    );
  }
}
