import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/models/category/category_model.dart';
import 'package:overone/widgets/buttons/clickable.dart';

class CategoriesListItem extends StatelessWidget {
  final CategoryModel category;
  final void Function(CategoryModel category) onPressed;
  final String Function(int count)? subTitleBuilder;
  final int Function(CategoryModel category)? getChildrenCount;

  const CategoriesListItem({
    Key? key,
    required this.category,
    required this.onPressed,
    this.subTitleBuilder,
    this.getChildrenCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: this,
      child: _CategoryButton(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20.0, top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _CategoryTitle(
                        title: category.title,
                      ),
                      const SizedBox(height: 4),
                      if (subTitleBuilder != null && getChildrenCount != null)
                        _SubCategoriesCount(
                          count: getChildrenCount!(category),
                        ),
                    ],
                  ),
                  Icon(
                    Icons.chevron_right,
                    color: Color(0xFF808486),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 8),
            Container(
              color: Color(0xFF808486),
              height: 1,
            ),
          ],
        ),
      ),
    );
  }
}

class _CategoryButton extends StatelessWidget {
  final Widget child;

  const _CategoryButton({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final props = context.read<CategoriesListItem>();
    return Clickable(
      onPressed: () {
        props.onPressed(props.category);
      },
      child: child,
    );
  }
}

class _CategoryTitle extends StatelessWidget {
  final String title;

  const _CategoryTitle({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        fontWeight: FontWeight.w600,
        fontSize: 14,
        height: 18 / 14,
        color: Color(0xFF151515),
      ),
    );
  }
}

class _SubCategoriesCount extends StatelessWidget {
  final int count;

  const _SubCategoriesCount({
    Key? key,
    required this.count,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final props = context.read<CategoriesListItem>();
    return Text(
      props.subTitleBuilder!(count),
      style: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 12,
        height: 18 / 12,
        color: Color(0xFF808486),
      ),
    );
  }
}
