import 'package:flutter/material.dart';
import 'package:overone/models/category/category_model.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/categories/categories_list/categories_list_item.dart';

class CategoriesList extends StatelessWidget {
  final List<CategoryModel>? categories;
  final void Function(CategoryModel category) onPressed;

  const CategoriesList({
    Key? key,
    required this.categories,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    if (categories == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return ListView.builder(
        itemCount: categories?.length ?? 0,
        itemBuilder: (context, index) {
          return CategoriesListItem(
            category: categories![index],
            subTitleBuilder: (count) => l10n.subCategoriesCount(count),
            getChildrenCount: (category) => category.subcategoriesCount,
            onPressed: onPressed,
          );
        },
      );
    }
  }
}
