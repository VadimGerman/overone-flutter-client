import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:overone/bloc/article_actions/article_actions_block.dart';
import 'package:overone/models/article/article_model.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/articles/article_link.dart';

class ArticlePreview extends StatelessWidget {
  final ArticleModel article;
  final List<Widget> actionButtons;

  const ArticlePreview({
    Key? key,
    required this.article,
    this.actionButtons = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authentication = context.authentication;
    return RepositoryProvider.value(
      value: this,
      child: BlocProvider(
        create: (context) => ArticleActionsBloc(authentication: authentication),
        child: ArticleLink(
          article: article,
          child: LayoutBuilder(
            builder: (context, constraints) {
              return _Wrapper(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _Image(
                      image: article.imageUrl,
                    ),
                    const SizedBox(width: 18),
                    SizedBox(
                      width: constraints.maxWidth - 56 - 18 - 32,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _Title(
                            title: article.title,
                          ),
                          const SizedBox(height: 5),
                          _Description(
                            description: article.description,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  _Source(
                                    source: article.source,
                                  ),
                                  const SizedBox(width: 9),
                                  _PublicationTime(
                                    putDate: DateTime.fromMillisecondsSinceEpoch(
                                      article.putDate.ceil() * 1000,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: actionButtons,
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class _Wrapper extends StatelessWidget {
  final Widget child;

  const _Wrapper({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(
            left: 16,
            right: 16,
            top: 9,
            bottom: 8,
          ),
          child: child,
        ),
        Divider(
          height: 2,
          color: Color(0xFFDADADA),
        ),
      ],
    );
  }
}

class _Title extends StatelessWidget {
  final String title;

  const _Title({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 32,
      child: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.w600,
          height: 16 / 14,
        ),
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}

class _Description extends StatelessWidget {
  final String description;

  const _Description({
    Key? key,
    required this.description,
  }) : super(key: key);

  String _clearString(String value) {
    return description.replaceAll(RegExp(r"\s+"), ' ');
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      _clearString(description),
      overflow: TextOverflow.ellipsis,
      maxLines: 3,
      style: TextStyle(
        color: Color(0xFF979D9F),
      ),
    );
  }
}

class _Source extends StatelessWidget {
  final String source;

  const _Source({
    Key? key,
    required this.source,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 85,
      child: Text(
        source,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          fontSize: 11,
          fontWeight: FontWeight.w700,
          height: 18 / 14,
        ),
      ),
    );
  }
}

class _PublicationTime extends StatelessWidget {
  final DateTime putDate;

  const _PublicationTime({
    Key? key,
    required this.putDate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String publicationTime;
    final DateTime now = DateTime.now();
    if (putDate.year == now.year) {
      if (putDate.month == now.month && putDate.day == now.day) {
        publicationTime = '${context.l10n.today} ${DateFormat('HH:mm').format(putDate)}';
      } else if (putDate.month == now.subtract(Duration(days: 1)).month && putDate.day == now.subtract(Duration(days: 1)).day) {
        publicationTime = '${context.l10n.yesterday} ${DateFormat('HH:mm').format(putDate)}';
      } else {
        publicationTime = DateFormat('dd.MM HH:mm').format(putDate);
      }
    } else {
      publicationTime = DateFormat('dd.MM.yy').format(putDate);
    }
    return Row(
      children: [
        Icon(
          Icons.access_time,
          color: Color(0xFF808486),
          size: 15,
        ),
        const SizedBox(width: 6),
        Text(
          publicationTime,
          style: TextStyle(color: Color(0xFF808486), fontSize: 11, height: 1.3),
        ),
      ],
    );
  }
}

class _Image extends StatelessWidget {
  final String image;

  const _Image({Key? key, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const size = 56.0;
    if (Uri.tryParse(image) == null) {
      return SizedBox(
        width: size,
        height: size,
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(4.0),
        child: CachedNetworkImage(
          width: size,
          height: size,
          placeholder: (context, url) => const CircularProgressIndicator(),
          imageUrl: image,
          errorWidget: (context, _, __) => const SizedBox(),
        ),
      );
    }
  }
}
