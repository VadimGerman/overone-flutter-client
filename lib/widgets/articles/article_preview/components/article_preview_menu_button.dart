import 'package:flutter/material.dart';
import 'package:overone/models/article/article_model.dart';
import 'package:overone/widgets/buttons/drop_menu_button.dart';

class ArticlePreviewMenuButton extends StatelessWidget {
  final ArticleModel article;
  final List<DropMenuButtonItem> dropMenuButtonItems;

  const ArticlePreviewMenuButton({
    Key? key,
    required this.article,
    required this.dropMenuButtonItems,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropMenuButton(
      items: dropMenuButtonItems,
      iconColor: Color(0xFF979D9F),
      iconSize: 15,
    );
  }
}