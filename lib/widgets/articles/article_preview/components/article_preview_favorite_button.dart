import 'package:flutter/material.dart';
import 'package:overone/models/article/article_model.dart';

class ArticlesPreviewFavoriteButton extends StatelessWidget {
  final ArticleModel article;

  const ArticlesPreviewFavoriteButton({
    Key? key,
    required this.article,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {},
      splashRadius: 15,
      icon: Icon(
        Icons.bookmark_border,
        color: Color(0xFF979D9F),
        size: 20,
      ),
    );
  }
}
