import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/articles/articles_bloc.dart';
import 'package:overone/models/article/article_model.dart';
import 'package:overone/models/pagination/pagination_model.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/articles/article_preview/article_preview.dart';

class ArticlesGrid extends StatelessWidget {
  final ArticlesBloc articlesBloc;
  final Widget Function(BuildContext context, ArticleModel article) articleBuilder;

  const ArticlesGrid({
    Key? key,
    required this.articlesBloc,
    required this.articleBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        const articleWidth = 360.0;
        final crossAxisCount = (constraints.maxWidth / (articleWidth + 30)).ceil() - 1;
        return BlocProvider.value(
          value: articlesBloc,
          child: BlocBuilder<ArticlesBloc, ArticlesState>(
            builder: (context, state) {
              final articlesBloc = context.read<ArticlesBloc>();
              if (state.articles == null) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state.articles!.isEmpty) {
                return SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  child: ConstrainedBox(
                    constraints: constraints,
                    child: Center(
                      child: Text(context.l10n.emptyNews),
                    ),
                  ),
                );
              } else {
                return Scrollbar(
                  child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: crossAxisCount,
                      mainAxisExtent: 193,
                    ),
                    itemCount: state.articles?.length ?? 0,
                    itemBuilder: (context, index) {
                      if (articlesBloc.needLoadMore(index)) {
                        articlesBloc.add(ArticlesEvent.load());
                      }
                      return Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 47.0),
                          child: SizedBox(
                            width: articleWidth,
                            child: articleBuilder(context, state.articles![index]),
                          ),
                        ),
                      );
                    },
                  ),
                );
              }
            },
          ),
        );
      },
    );
  }
}
