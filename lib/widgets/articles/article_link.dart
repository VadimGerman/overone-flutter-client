import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/bloc/article_actions/article_actions_block.dart';
import 'package:overone/models/article/article_model.dart';
import 'package:overone/widgets/buttons/clickable.dart';
import 'package:url_launcher/url_launcher.dart';

class ArticleLink extends StatelessWidget {
  final Widget child;
  final ArticleModel article;

  const ArticleLink({
    Key? key,
    required this.child,
    required this.article,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Clickable(
      onPressed: () async {
        if (!await launchUrl(Uri.parse(article.link))) throw 'Could not launch ${article.link}';
        context.read<ArticleActionsBloc>().add(ArticleActionsAddToHistoryEvent(articleId: article.id));
      },
      child: child,
    );
  }
}
