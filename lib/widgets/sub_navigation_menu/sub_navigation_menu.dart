import 'package:flutter/material.dart';
import 'package:overone/widgets/sub_navigation_menu/components/sub_navigation_menu_item.dart';

class SubNavigationMenu extends StatelessWidget {
  final List<SubNavigationMenuItem> items;

  const SubNavigationMenu({
    Key? key,
    required this.items,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: items,
    );
  }
}
