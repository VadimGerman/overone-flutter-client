import 'package:flutter/material.dart';

class SubNavigationMenuItem extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;
  final bool selected;

  const SubNavigationMenuItem({
    Key? key,
    required this.onPressed,
    required this.title,
    required this.selected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      splashColor: Color(0xFF0A93A6),
      child: Padding(
        padding: const EdgeInsets.only(
          top: 8,
          left: 32,
          right: 32,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              title,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w600,
                color: selected ? Colors.black : Color(0xFF8D9192),
              ),
            ),
            const SizedBox(height: 5),
            Container(
              width: 48,
              height: 2,
              color: selected ? Color(0xFF0A93A6) : Colors.transparent,
            ),
          ],
        ),
      ),
    );
  }
}
