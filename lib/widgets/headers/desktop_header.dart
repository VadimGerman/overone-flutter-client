import 'package:flutter/material.dart';
import 'package:overone/bloc/authentication/authentication_bloc.dart';
import 'package:overone/widgets/forms/search_field.dart';
import 'package:overone/widgets/navigation_menu/navigation_menu_desktop.dart';
import 'package:provider/src/provider.dart';

class DesktopHeader extends StatelessWidget {

  final Widget? child;

  const DesktopHeader({
    Key? key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 13.0,
        right: 13.0,
        top: 18,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          NavigationMenuDesktop(),
          if(child != null) child!,
          Row(
            children: [
              SearchField(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 70.0),
                child: _UserAvatar(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _UserAvatar extends StatelessWidget {
  const _UserAvatar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authenticationBloc = context.read<AuthenticationBloc>();
    final user = authenticationBloc.state.userInfo!;
    return Column(
      children: [
        CircleAvatar(
          radius: 26,
          backgroundImage: NetworkImage(user.avatar),
        ),
        const SizedBox(height: 8),
        SizedBox(
          width: 60,
          child: Text(
            user.login,
            style: TextStyle(fontWeight: FontWeight.w700),
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }
}

