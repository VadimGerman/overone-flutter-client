import 'package:flutter/material.dart';
import 'package:overone/bloc/authentication/authentication_bloc.dart';
import 'package:overone/utils/build_context_extension.dart';
import 'package:overone/widgets/forms/search_field.dart';
import 'package:overone/widgets/navigation_menu/navigation_menu_desktop.dart';
import 'package:provider/src/provider.dart';

class MobileHeader extends StatelessWidget {

  final Widget? child;

  const MobileHeader({
    Key? key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        left: 20,
        right: 20,
        top: 36
      ),
      decoration: BoxDecoration(
        color: Color(0xFFF4F4F4),
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(24),
          bottomLeft: Radius.circular(24),
        )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          SearchField(),
          const SizedBox(height: 24),
          if(child != null) child!,
        ],
      ),
    );
  }
}
