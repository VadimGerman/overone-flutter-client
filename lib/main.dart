import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:overone/app.dart';
import 'package:overone/bloc/app_bloc_observer.dart';

void main() async {
  final widgetsBinding = WidgetsFlutterBinding.ensureInitialized();

  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

  await Future.delayed(Duration(milliseconds: 500));

  HydratedBlocOverrides.runZoned(
    () => runApp(App()),
    createStorage: () async {
      return HydratedStorage.build(
        storageDirectory: kIsWeb ? HydratedStorage.webStorageDirectory : await getApplicationDocumentsDirectory(),
      );
    },
    blocObserver: AppBlocObserver(),
  );

  Future(() => FlutterNativeSplash.remove());
}
