import 'package:auto_route/auto_route.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:overone/bloc/authentication/authentication_bloc.dart';
import 'package:overone/bloc/localization/localization_bloc.dart';
import 'package:overone/bloc/themes/themes_bloc.dart';
import 'package:overone/router/app_router.dart';
import 'package:overone/router/app_router_observer.dart';
import 'package:overone/router/guards/auth_guard.dart';
import 'package:overone/router/guards/un_auth_guard.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  AppRouter? appRouter;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => LocalizationBloc(
            languageCode: Intl.getCurrentLocale(),
          ),
          lazy: false,
        ),
        BlocProvider(
          create: (context) => AuthenticationBloc(),
          lazy: false,
        ),
        BlocProvider(
          create: (context) => ThemesBloc(
            brightness: Brightness.light,
          ),
          lazy: false,
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<AuthenticationBloc, AuthenticationState>(
            listenWhen: (p, n) => p.authentication != n.authentication,
            listener: (context, state) {
              if (state.authentication != null) {
                appRouter?.push(NewsRoute());
              } else {
                appRouter?.push(LoginRoute());
              }
            },
          ),
        ],
        child: Builder(
          builder: (context) {
            final languageCode = context.select<LocalizationBloc, String>((bloc) => bloc.state.languageCode);
            final themeData = context.select<ThemesBloc, ThemeData>((bloc) => bloc.state.theme);
            appRouter ??= AppRouter(
              authGuard: AuthGuard(
                authenticationBloc: context.read<AuthenticationBloc>(),
              ),
              unAuthGuard: UnAuthGuard(
                authenticationBloc: context.read<AuthenticationBloc>(),
              ),
            );
            return MaterialApp.router(
              routeInformationParser: appRouter!.defaultRouteParser(),
              routerDelegate: AutoRouterDelegate(
                appRouter!,
                navigatorObservers: () => [AppRouterObserver()],
              ),
              localizationsDelegates: AppLocalizations.localizationsDelegates,
              supportedLocales: AppLocalizations.supportedLocales,
              locale: Locale(languageCode),
              theme: themeData,
              scrollBehavior: MaterialScrollBehavior().copyWith(
                dragDevices: {
                  PointerDeviceKind.mouse,
                  PointerDeviceKind.touch,
                  PointerDeviceKind.stylus,
                  PointerDeviceKind.unknown,
                },
              ),
            );
          },
        ),
      ),
    );
  }
}