import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:overone/router/guards/auth_guard.dart';
import 'package:overone/router/guards/un_auth_guard.dart';
import 'package:overone/screens/categories/categories_screen.dart';
import 'package:overone/screens/categories/mobile/category_feed_screen.dart';
import 'package:overone/screens/categories/mobile/sub_categories_mobile_screen.dart';
import 'package:overone/screens/folders/folders_screen.dart';
import 'package:overone/screens/folders/mobile/folder_mobile_screen.dart';
import 'package:overone/screens/login_screen.dart';
import 'package:overone/screens/news/children/news_favorites_screen.dart';
import 'package:overone/screens/news/children/news_my_feed_screen.dart';
import 'package:overone/screens/news/news_screen.dart';
import 'package:overone/screens/favorites/children/favorites_bookmarks_screen.dart';
import 'package:overone/screens/favorites/children/favorites_history_screen.dart';
import 'package:overone/screens/favorites/favorites_screen.dart';
import 'package:overone/screens/profile_screen.dart';
import 'package:overone/screens/register_screen.dart';

part 'app_router.gr.dart';

@CustomAutoRouter(
  replaceInRouteName: 'Screen,Route',
  routes: <AutoRoute>[
    AutoRoute(
      initial: true,
      page: LoginScreen,
      path: '/login',
      guards: [
        UnAuthGuard,
      ],
    ),
    AutoRoute(
      page: RegisterScreen,
      path: '/register',
      guards: [
        UnAuthGuard,
      ],
    ),
    CustomRoute(
      page: NewsScreen,
      path: '/news',
      transitionsBuilder: TransitionsBuilders.noTransition,
      children: [
        CustomRoute(
          page: NewsMyFeedScreen,
          path: '',
          transitionsBuilder: TransitionsBuilders.noTransition,
        ),
        CustomRoute(
          page: NewsFavoritesScreen,
          path: 'favorites',
          transitionsBuilder: TransitionsBuilders.noTransition,
        ),
      ],
      guards: [
        AuthGuard,
      ]
    ),
    CustomRoute(
      page: CategoriesScreen,
      path: '/categories',
      transitionsBuilder: TransitionsBuilders.noTransition,
      guards: [
        AuthGuard,
      ],
    ),
    CustomRoute(
      page: SubCategoriesMobileScreen,
      path: '/category/:categoryId/subcategories',
      transitionsBuilder: TransitionsBuilders.noTransition,
      guards: [
        AuthGuard,
      ],
    ),
    CustomRoute(
      page: CategoryFeedScreen,
      path: '/category/:categoryId/subcategory/:subCategoryId/feeds/:feedId',
      transitionsBuilder: TransitionsBuilders.noTransition,
      usesPathAsKey: true,
      guards: [
        AuthGuard,
      ],
    ),
    CustomRoute(
      page: FoldersScreen,
      path: '/folders',
      transitionsBuilder: TransitionsBuilders.noTransition,
      guards: [
        AuthGuard,
      ],
    ),
    CustomRoute(
      page: FolderMobileScreen,
      path: '/folder_mobile/:folderId',
      transitionsBuilder: TransitionsBuilders.noTransition,
      guards: [
        AuthGuard,
      ],
    ),
    CustomRoute(
      page: ProfileScreen,
      path: '/profile',
      transitionsBuilder: TransitionsBuilders.noTransition,
      guards: [
        AuthGuard,
      ],
    ),
    CustomRoute(
      page: FavoritesScreen,
      path: '/favorites',
      transitionsBuilder: TransitionsBuilders.noTransition,
      guards: [
        AuthGuard,
      ],
      children: [
        CustomRoute(
          page: FavoritesBookmarksScreen,
          path: '',
          transitionsBuilder: TransitionsBuilders.noTransition,
        ),
        CustomRoute(
          page: FavoritesHistoryScreen,
          path: 'history',
          transitionsBuilder: TransitionsBuilders.noTransition,
        ),
      ],
    ),
  ],
)
class AppRouter extends _$AppRouter {
  AppRouter({
    required AuthGuard authGuard,
    required UnAuthGuard unAuthGuard,
  }) : super(
          authGuard: authGuard,
          unAuthGuard: unAuthGuard,
        );
}
