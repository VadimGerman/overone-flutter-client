import 'package:auto_route/auto_route.dart';
import 'package:overone/bloc/authentication/authentication_bloc.dart';
import 'package:overone/router/app_router.dart';

class UnAuthGuard extends AutoRouteGuard {

  final AuthenticationBloc authenticationBloc;

  UnAuthGuard({
    required this.authenticationBloc,
  });

  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) {
    if(authenticationBloc.state.authentication != null) {
      router.push(NewsRoute());
    } else {
      resolver.next(true);
    }
  }
}