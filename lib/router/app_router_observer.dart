import 'dart:developer';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

class AppRouterObserver extends AutoRouterObserver {
  @override
  void didInitTabRoute(TabPageRoute route, TabPageRoute? previousRoute) {
    log('Router init tab: ${previousRoute?.name} -> ${route.name}');
  }
  @override
  void didChangeTabRoute(TabPageRoute route, TabPageRoute previousRoute) {
    log('Router chnage tab: ${previousRoute.name} -> ${route.name}');
  }

  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    log('Router push: ${previousRoute?.settings.name} -> ${route.settings.name}');
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    log('Router pop: ${previousRoute?.settings.name} -> ${route.settings.name}');
  }

  @override
  void didRemove(Route<dynamic> route, Route<dynamic>? previousRoute) {
    log('Router remove: ${previousRoute?.settings.name} -> ${route.settings.name}');
  }

  @override
  void didReplace({ Route<dynamic>? newRoute, Route<dynamic>? oldRoute }) {
    log('Router replace: ${oldRoute?.settings.name} -> ${newRoute?.settings.name}');
  }

  @override
  void didStartUserGesture(Route<dynamic> route, Route<dynamic>? previousRoute) {
    log('Router start user gesture: ${previousRoute?.settings.name} -> ${route.settings.name}');
  }

  @override
  void didStopUserGesture() {
    log('Router stop user gesture');
  }
}